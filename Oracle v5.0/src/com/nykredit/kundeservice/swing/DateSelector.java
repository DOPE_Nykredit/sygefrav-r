package com.nykredit.kundeservice.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import com.nykredit.kundeservice.util.Date;
import com.nykredit.kundeservice.util.SF;

public class DateSelector extends JPanel {
	private static final long serialVersionUID = 1L;

	private SF sf = new SF() {public void func() {}};
	private boolean[] days = {true,true,true,true,true,false,true};
	private JComboBox periodComboBox = null;
	private JLabel  sDateLabel = null,
					eDateLabel = null,
					chosenPeriodLabel = null;
	private JButton buttonUp = null,
					buttonDown = null;
	private Date 	sDate = new Date(),
					eDate = new Date();
	private String cho = "";
	private boolean withEnd = true;
	private char index = 'D';
	private static boolean showDay = true;
	private static boolean showWeek = true;
	private static boolean showMonth = true;
	private static boolean showYear = true;
	private static boolean showAdhoc = true;

	/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
	 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Public Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
	 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
	 */
	
	public static void main(String[] args) {
		try {
	        UIManager.setLookAndFeel(
	            UIManager.getSystemLookAndFeelClassName());
	    } catch (Exception e) {}
		
		JFrame test = new JFrame();
		test.setContentPane(new DateSelector());
		test.pack();
		test.setVisible(true);
	}
	
	public DateSelector(int x,int y,int width,int height){
		this.setBounds(x,y,width,height);
		initialize();
	}

	public DateSelector(boolean showDay,boolean showWeek,boolean showMonth,boolean showYear,boolean showAdhoc){
		DateSelector.showDay = showDay;
		DateSelector.showWeek = showWeek;
		DateSelector.showMonth = showMonth;
		DateSelector.showYear = showYear;
		DateSelector.showAdhoc = showAdhoc;
		initialize();
		periodComboBox.setSelectedIndex(0);
	}
	
	
	public DateSelector(){
		initialize();
	}
	
	private void initialize(){
		change(-1);
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 3;
		c.gridheight = 1;
		c.weightx = 1;
		c.weighty = 1;
		c.fill = GridBagConstraints.BOTH;
		this.add(createPeriodComboBox(), c);
		sDateLabel = getLabel(71, 20, sDate.ToString("dd-MM-yyyy"),0,0,1,0);
		eDateLabel = getLabel(71, 20, eDate.ToString("dd-MM-yyyy"),1,1,1,0);
		chosenPeriodLabel = getLabel(90, 40, cho,0,1,1,1);
		c.gridx = 0;
		c.gridy = 1;	
		c.gridwidth = 1;
		c.gridheight = 1;

		c.fill = GridBagConstraints.BOTH;
		this.add(sDateLabel,c);
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.BOTH;
		this.add(eDateLabel,c);
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 1;
		c.gridheight = 2;
		c.fill = GridBagConstraints.BOTH;
		this.add(chosenPeriodLabel,c);
		c.gridx = 2;
		c.gridy = 1;
		c.gridwidth = 1;
		c.gridheight = 1;

		c.fill = GridBagConstraints.BOTH;
		this.add(getButtonUp(),c);
		c.gridx = 2;
		c.gridy = 2;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = GridBagConstraints.BOTH;
		this.add(getButtonDown(),c);

	}
	

	public void setPeriodComboBox(JComboBox periodComboBox){
		this.periodComboBox = periodComboBox;
	}

	public JComboBox getPeriodComboBox(){
		return this.periodComboBox;
	}
	
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Private Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
	private void change(int value) {
		if (index == 'A') {if (value != 0) {
			if (value > 0) {
				Calendar lol = new Calendar(sDate.ToString(),days[0],days[1],days[2],days[3],days[4],days[5],days[6]);
				sDate.override(lol.getDate());
				lol = null;
			} else if (withEnd) {
				Calendar lol = new Calendar(eDate.ToString());
				eDate.override(lol.getDate());
				lol = null;
			}
		} } else {
			do {
				sDate.edit(index, value);
				sDate.trunc(index);
				eDate.override(sDate.ToString());
				eDate.edit(index, 1);
				eDate.edit('D', -1);
			} while (index == 'D' & !days[sDate.getInfo('B')-1] & value != 0);
		}
		switch (index) {
		case 'Y': cho = Integer.toString(sDate.getInfo('Y'));			break;
		case 'M': cho = sDate.getName('M');								break;
		case 'W': cho = "Uge " + Integer.toString(sDate.getInfo('W'));	break;
		case 'D': cho = sDate.getName('D');								break;
		default:  cho = "";												break;
		}
	}
	private void updateText() {
		sDateLabel.setText(sDate.ToString("dd-MM-yyyy"));
		eDateLabel.setText(eDate.ToString("dd-MM-yyyy"));
		chosenPeriodLabel.setText(cho);
		sf.func();
	}

	private JLabel getLabel(int w, int h, String text,int top,int bottom,int left,int right) {
		JLabel lab = new JLabel(text,SwingConstants.CENTER);
		lab.setBorder(BorderFactory.createMatteBorder(top, left, bottom, right, Color.gray));
		lab.setPreferredSize(new Dimension(w, h));
		lab.setMinimumSize(new Dimension(w, h));
		lab.setOpaque(true);
		lab.setBackground(Color.white);
		return lab;
	}
	
	private JComboBox createPeriodComboBox() {
		periodComboBox = new JComboBox();
		periodComboBox.setFocusable(false);
		periodComboBox.setBorder(BorderFactory.createLineBorder(Color.gray));
		periodComboBox.setMinimumSize(new Dimension(180,20));
		periodComboBox.setPreferredSize(new Dimension(180,20));
		if(showDay)periodComboBox.addItem("Dag");
		if(showWeek)periodComboBox.addItem("Uge");
		if(showMonth)periodComboBox.addItem("M�ned");
		if(showYear)periodComboBox.addItem("�r");
		if(showAdhoc)periodComboBox.addItem("Ad-hoc");
		
		periodComboBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				if (periodComboBox.getSelectedItem() == "Dag") 		{index = 'D';}
				if (periodComboBox.getSelectedItem() == "Uge") 		{index = 'W';}
				if (periodComboBox.getSelectedItem() == "M�ned")	{index = 'M';}
				if (periodComboBox.getSelectedItem() == "�r") 		{index = 'Y';}
				if (periodComboBox.getSelectedItem() == "Ad-hoc") 	{index = 'A';}
				change(0);
				updateText();
			}
		});
		return periodComboBox;
	}
	
	private JButton getButtonUp() {
		buttonUp = new JButton("\u25B2");
		buttonUp.setMargin(new Insets(1, 1, 1, 1));
		buttonUp.setPreferredSize(new Dimension(20, 20));
		buttonUp.setMinimumSize(new Dimension(20, 20));
		buttonUp.setBorderPainted(true);
		buttonUp.setFocusPainted(false);
		buttonUp.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				change(1);
				updateText();
			}
			
			
			
		});
		return buttonUp;
	}
	private JButton getButtonDown() {
		buttonDown = new JButton("\u25BC");
		buttonDown.setMargin(new Insets(1, 1, 1, 1));
		buttonDown.setPreferredSize(new Dimension(20, 20));
		buttonDown.setMinimumSize(new Dimension(20, 20));
		buttonDown.setBorderPainted(true);
		buttonDown.setFocusPainted(false);
		buttonDown.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				change(-1);
				updateText();
			}
		});
		return buttonDown;
	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Public Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */	
	public String getStart() {
		return sDate.ToString("yyyy-MM-dd");
	}
	public String getEnd() {
		return eDate.ToString("yyyy-MM-dd");
	}
	public String getPeriode() {
		return cho;
	}
	public char getValg() {
		return index;
	}
	public void setEvent(final SF sf) {
		this.sf = sf;
	}
	public void setEndDateEnabled(boolean to) {
		withEnd = to;
		eDateLabel.setEnabled(to);
	}
	public void setStartup(int index) {
		if (index > 0 & index < 6)
			periodComboBox.setSelectedIndex(index);
	}
	public void setDays(boolean ma, boolean ti, boolean on, boolean to, boolean fr, boolean l�, boolean s�) {
		if (!(ma | ti | on | to | fr | l� | s�)) ma = true;
		this.days = new boolean[] {ma,ti,on,to,fr,l�,s�};
		change(-1);
		updateText();
	}
}