package com.nykredit.kundeservice.swing;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class NTableRenderer extends DefaultTableCellRenderer {
	private static final long serialVersionUID = 1L;
	
	private Color zebra1 = new Color(214,214,214);
	private Color zebra2 = new Color(246,246,246);

	public Component getTableCellRendererComponent (JTable table,Object obj, boolean isSelected, boolean hasFocus, int row, int column) {
		Component cell = super.getTableCellRendererComponent(table, obj, isSelected, hasFocus, row, column);

  		cell.setForeground(Color.black);
	  	if (row%2 == 0) { cell.setBackground(zebra1); }
	  	else 			{ cell.setBackground(zebra2); }
		return cell;
    }
	
	public void setZebraColor(Color zebra1,Color zebra2) {
		if (zebra1 == null) this.zebra1 = new Color(255,255,255);
		else this.zebra1 = zebra1;
		if (zebra2 == null) this.zebra2 = new Color(255,255,255);
		else this.zebra2 = zebra2;
	}
	
	public void addMarking(Component cell,boolean isSelected,Color marker,int proc) {
		if (isSelected) {
			if (marker == null) marker = new Color(127,127,127);
			if (proc < 0) proc = 0;
			if (proc > 100) proc = 100;
			Color X = cell.getBackground();
			int R = marker.getRed() + (X.getRed()-marker.getRed()) * proc / 100;
			int G = marker.getGreen() + (X.getGreen()-marker.getGreen()) * proc / 100;
			int B = marker.getBlue() + (X.getBlue()-marker.getBlue()) * proc / 100;
			cell.setBackground(new Color(R,G,B));
			cell.setForeground(new Color(255,255,255));
		}
	}
}