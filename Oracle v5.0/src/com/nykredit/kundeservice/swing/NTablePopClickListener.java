package com.nykredit.kundeservice.swing;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

class NTablePopClickListener extends MouseAdapter {
	NTable table;
	
	public NTablePopClickListener(NTable table) {
		this.table = table;
	}
	
	public void mousePressed(MouseEvent e){
		if (e.isPopupTrigger())
			doPop(e);
		}
	public void mouseReleased(MouseEvent e){
		if (e.isPopupTrigger())
			doPop(e);
		}
	private void doPop(MouseEvent e){
		
		NTableMenu menu = new NTableMenu(this.table);
		
		menu.show(e.getComponent(), e.getX(), e.getY());
	}
}