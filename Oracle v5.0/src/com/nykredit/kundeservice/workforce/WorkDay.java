package com.nykredit.kundeservice.workforce;
import java.util.ArrayList;

import org.joda.time.Interval;
import org.joda.time.LocalDate;

import com.nykredit.kundeservice.util.TimeOutOfBoundsException;

public class WorkDay implements Comparable<WorkDay> {
	
	private LocalDate date;
	private Interval span;
	
	private int workMinutes;
	private int paidMinutes;
	
	private ArrayList<ExceptionInWorkDay> exceptions = new ArrayList<ExceptionInWorkDay>();
	
	public LocalDate getDate(){
		return this.date;
	}
	
	public Interval getSpan(){
		return this.span;
	}
	
	public int getWorkMinutes(){
		return this.workMinutes;
	}
	public int getPaidMinutes(){
		return this.paidMinutes;
	}
	
	public boolean hasFullDayException(){
		if (this.exceptions.size() == 1 && this.exceptions.get(0).getSpan() == null)
			return true;
		else
			return false;
	}
	public boolean hasExceptionCoveringEntireShift(){
		for (ExceptionInWorkDay e : this.exceptions)
			if (e.getSpan().equals(this.span))
				return true;
		
		return false;
	}
	public boolean hasExceptionOfType(ExceptionInWorkDay.WorkdayExceptionType exceptionType){
		for(ExceptionInWorkDay w : this.exceptions)
			if(w.getType() == exceptionType)
				return true;
		
		return false;
	}
	
	public ArrayList<ExceptionInWorkDay> getExceptions(){
		return this.exceptions;
	}
	
	public boolean hasAttendence(){
		if (this.workMinutes > 0)
			return true;
		else
			for (ExceptionInWorkDay e : this.exceptions)
				if (e.getType().isCountedAsAttendence())
					return true;
		
		return false;			
	}
	
	public WorkDay(LocalDate date, Interval span, int workMinutes, int paidMinutes) {
		if (date == null)
			throw new IllegalArgumentException("Date cannot be null.");
			
		this.date = date;
		this.span = span;
		this.workMinutes = workMinutes;
		this.paidMinutes = paidMinutes;
	}
	public WorkDay(LocalDate date, Interval span, int workMinutes, int paidMinutes, int dayType, int fullDayExceptionType, String[] stateData) throws TimeOutOfBoundsException {
		this(date, span, workMinutes, paidMinutes);
		
    	if (dayType == 1){
    		this.addWorkdayException(new ExceptionInWorkDay(null, 1, 2));
    	} else if (dayType == 2){
    		this.addWorkdayException(new ExceptionInWorkDay(null, 0, 2));
    	} else if (dayType == 3){
    		this.addWorkdayException(new ExceptionInWorkDay(null, fullDayExceptionType, 3));
    	} else if (dayType == 4){
    		if (stateData != null)
		    	for (String d : stateData)
		    		this.addWorkdayException(new ExceptionInWorkDay(d));				    			
    	} else if (dayType == 5){
    		this.addWorkdayException(new ExceptionInWorkDay(null, 1, 2));
    	}
	}

	public void addWorkdayException(ExceptionInWorkDay exception){
		this.exceptions.add(exception);
	}

	@Override
	public int compareTo(WorkDay arg0) {
		if (this == arg0) 
			return 0;
		
		return (this.date.compareTo(arg0.getDate()));
	}
}
