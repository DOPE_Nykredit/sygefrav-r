package com.nykredit.kundeservice.awt;

public class Color {

    /**
     * Converts a given color to its negative
     * 
     * @param color
     * @return
     */
	public java.awt.Color negativeColor(java.awt.Color color){
		
		int oldRed = color.getRed();
		int oldGreen = color.getGreen();
		int oldBlue = color.getBlue();
		
		int newRed = 255 -oldRed;
		int newGreen = 255 -oldGreen;
		int newBlue = 255 -oldBlue;
		
		return new java.awt.Color(newRed,newGreen,newBlue);
	}
	
	
}
