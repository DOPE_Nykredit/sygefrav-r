package com.nykredit.kundeservice.data.sql;

import com.nykredit.kundeservice.data.DopeException;

public class DopeResultSetException extends DopeException {

	private static final long serialVersionUID = 1L;

	public DopeResultSetException(String exceptionMsg, String dopeErrorMsg){
		super(null, exceptionMsg, dopeErrorMsg);
	}
	public DopeResultSetException(Exception e, String exceptionMsg,	String dopeErrorMsg){
		super(e, exceptionMsg, dopeErrorMsg);
	}
}
