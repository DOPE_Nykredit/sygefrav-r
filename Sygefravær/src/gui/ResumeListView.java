package gui;

import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;

import absenceAnalysis.AgentSicknessAbsenceResume;

public class ResumeListView extends JTable {
	
	public enum ResumeFilter{
		SHOW_ALL,
		SHOW_PERIODLIMIT_EXCEEDED
	}

	private static final long serialVersionUID = 1L;
	
	private ArrayList<AgentSicknessAbsenceResume> resumes = new ArrayList<AgentSicknessAbsenceResume>();
	
	private ResumeFilter filter = ResumeFilter.SHOW_PERIODLIMIT_EXCEEDED;
	
	private int sicknessPeriodLimitCount = 3;
	private int numberOfMonthsInPeriod = 3; 
	private int maxNumberOfSickdaysInPeriod = 3;
	
	public ResumeFilter getFilter(){
		return this.filter;
	}
	public void setFilter(ResumeFilter filter){
		this.filter = filter;
		
		this.updateList();
	}
	
	public AgentSicknessAbsenceResume getSelectedResume(){
		return ((ResumeListViewTableModel)this.getModel()).getResume(this.convertRowIndexToModel(this.getSelectedRow()));
	}
	
	public void setData(ArrayList<AgentSicknessAbsenceResume> resumes){
		this.resumes = resumes;
		
		this.updateList();
	}
	
	public ResumeListView(){
		this.setRowSelectionAllowed(true);
		this.setColumnSelectionAllowed(false);
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.setShowGrid(false);
	}
	
	public void updateList(){
		if(this.filter == ResumeFilter.SHOW_ALL)
			this.setModel(new ResumeListViewTableModel(this.resumes.toArray(new AgentSicknessAbsenceResume[0])));
		else{
			ArrayList<AgentSicknessAbsenceResume> dueForNotification = new ArrayList<AgentSicknessAbsenceResume>();
			
			for(AgentSicknessAbsenceResume a : this.resumes)
				if(a.isDueForNotification(this.numberOfMonthsInPeriod, this.sicknessPeriodLimitCount, this.maxNumberOfSickdaysInPeriod))
					dueForNotification.add(a);
			
			this.setModel(new ResumeListViewTableModel(dueForNotification.toArray(new AgentSicknessAbsenceResume[0])));
		}
	}
	
	private class ResumeListViewTableModel extends AbstractTableModel{

		private static final long serialVersionUID = 1L;
		
		private AgentSicknessAbsenceResume[] data;
		private String[] columns = {"Initialer", "Team", "Navn"};
		
		public AgentSicknessAbsenceResume getResume(int rowIndex){
			if(this.data.length > rowIndex && rowIndex >= 0)
				return this.data[rowIndex];
			else
				return null;
		}
		
		@Override
		public int getColumnCount() {
			return this.columns.length;
		}
		@Override
        public String getColumnName(int columnIndex) {
            return this.columns[columnIndex];
        }

		@Override
		public int getRowCount() {
			return this.data.length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			switch (columnIndex){
				case 0:
					return this.data[rowIndex].getAgent().getInitials();
				case 1:
					return this.data[rowIndex].getAgent().getTeamCode();
				case 2:
					return this.data[rowIndex].getAgent().getFullName();
				default:
					return null;
			}
		}
		
		public ResumeListViewTableModel(AgentSicknessAbsenceResume[] data){
			this.data = data;
		}	
	}
}