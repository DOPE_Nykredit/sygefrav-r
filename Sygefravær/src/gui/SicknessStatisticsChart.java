package gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import org.jdesktop.swingx.VerticalLayout;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RectangleInsets;
import org.joda.time.LocalDate;

import com.nykredit.kundeservice.workforce.ExceptionInWorkDay;
import com.nykredit.kundeservice.workforce.WorkDay;
import com.nykredit.kundeservice.workforce.ExceptionInWorkDay.WorkdayExceptionType;

import absenceAnalysis.AbsenceAnalysisAgent;

public class SicknessStatisticsChart extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private LocalDate startDate = new LocalDate().withDayOfMonth(1);
	private AbsenceAnalysisAgent currentAgent;
	
	private ArrayList<AbsenceAnalysisAgent> data;
	
	private final JFreeChart chart = ChartFactory.createLineChart("Sygefrav�r", "M�ned", "Sygefrav�rsprocent", null, PlotOrientation.VERTICAL, true, false, false);
	private final JTable table = new JTable();
		
	public LocalDate getStartDate(){
		return this.startDate;
	}
	public void setStartDateMonth(int monthOfYear){
		this.startDate = this.startDate.withMonthOfYear(monthOfYear);
	}
	public void setStartDateYear(int year){
		this.startDate = this.startDate.withYear(year);
	}
	
	public AbsenceAnalysisAgent getCurrentAgent(){
		return this.currentAgent;
	}
	public void setCurrentAgent(AbsenceAnalysisAgent agent){
		this.currentAgent = agent;
	}
	
	public void setData(ArrayList<AbsenceAnalysisAgent> data){
		this.data = data;
	}
	
	public SicknessStatisticsChart(){		
		this.basicSetup();
	}
	public SicknessStatisticsChart(int startMonthOfYear, int startYear, ArrayList<AbsenceAnalysisAgent> data){
		this.startDate = new LocalDate(startYear, startMonthOfYear, 1);
		
		this.data = data;
		
		this.basicSetup();
	}
	private void basicSetup(){
		this.setLayout(new VerticalLayout());
		this.setPreferredSize(new Dimension(200, 100));
		this.setMaximumSize(new Dimension(200, 100));
		
		this.chart.getPlot().setBackgroundPaint(Color.WHITE);
		
		CategoryPlot plot = (CategoryPlot)this.chart.getPlot();
		plot.setBackgroundPaint(Color.WHITE);
		plot.setRangeGridlinePaint(Color.BLACK);
		
		LineAndShapeRenderer r = (LineAndShapeRenderer)plot.getRenderer();
		r.setSeriesPaint(0, Color.RED);
		r.setSeriesPaint(1, Color.BLUE);
		r.setSeriesPaint(2, Color.GREEN);
		r.setSeriesPaint(3, Color.ORANGE);
	
		r.setSeriesStroke(0, new BasicStroke(2));
		r.setSeriesStroke(1, new BasicStroke(2));
		r.setSeriesStroke(2, new BasicStroke(2));
		r.setSeriesStroke(3, new BasicStroke(2));
		
		CategoryAxis domainAxis = plot.getDomainAxis();
		domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
        domainAxis.setLowerMargin(0.0);
        domainAxis.setUpperMargin(0.0);
        
        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        rangeAxis.setAutoRangeIncludesZero(true);
		
        ChartPanel chartPanel = new ChartPanel(this.chart); 
		this.add(chartPanel);
		chart.setPadding(new RectangleInsets(10, 25, 10, 50));
		
		JScrollPane scrollPane = new JScrollPane(this.table);
		scrollPane.setBackground(Color.WHITE);
		this.add(scrollPane);
		
		this.table.getPreferredScrollableViewportSize().setSize(650, 65);
		this.table.setEnabled(false);
		
		this.chart.setTitle("Sygefrav�r");
	}
	
	public void updateChart(){
		DefaultCategoryDataset agentDataSet = new DefaultCategoryDataset();
		
		ArrayList<tableData> tableData = new ArrayList<SicknessStatisticsChart.tableData>();
		
		for(int i = 0; i < 13; i++)
			tableData.add(new tableData());			
		
		if(this.currentAgent != null){
			this.chart.setTitle("Sygefrav�r: " + this.currentAgent.getInitials() + " - " + this.currentAgent.getFullName());
			
			LocalDate dateIterator = LocalDate.fromDateFields(this.startDate.toDate());
					
			for(int i = 0; i < 12; i++){
				dateIterator = dateIterator.plusMonths(1);
				int toleratedSicknessPercentage;
				
				if(this.currentAgent.getTeamGroup().equals("Ekspedition"))
					toleratedSicknessPercentage = 4;
				else
					toleratedSicknessPercentage = 5;
				
				agentDataSet.setValue(Math.round(this.currentAgent.getSicknessPercentage(dateIterator.getMonthOfYear(), dateIterator.getYear())), 
								      this.currentAgent.getInitials(), 
									  dateIterator.toString("MMM") + "-" + dateIterator.toString("yyyy"));
				
				long teamSicknessPercentage = Math.round(this.getSicknessPercentageOnTeam(dateIterator.getMonthOfYear(), dateIterator.getYear()));
				long teamGroupSicknessPercentage = Math.round(this.getSicknessPercentageOnTeamGroup(dateIterator.getMonthOfYear(), dateIterator.getYear()));
												
				agentDataSet.setValue(teamGroupSicknessPercentage, "Gruppe " + this.currentAgent.getTeamGroup(), dateIterator.toString("MMM") + "-" + dateIterator.toString("yyyy"));
				agentDataSet.setValue(toleratedSicknessPercentage, "M�l", dateIterator.toString("MMM") + "-" + dateIterator.toString("yyyy"));
				agentDataSet.setValue(teamSicknessPercentage, "Team " + this.currentAgent.getTeamCode(), dateIterator.toString("MMM") + "-" + dateIterator.toString("yyyy"));
											
				tableData.get(i).monthAndYear = dateIterator;
				tableData.get(i).agentSicknessPercentage = this.currentAgent.getSicknessPercentage(dateIterator.getMonthOfYear(), dateIterator.getYear());
				tableData.get(i).teamSicknessPercentage = teamSicknessPercentage;
				tableData.get(i).teamGroupSicknessPercentage = teamGroupSicknessPercentage;
			}
			
			tableData.get(12).agentSicknessPercentage = this.currentAgent.getSicknessPercentage(this.startDate, 
																						  		LocalDate.now());
			
			tableData.get(12).teamSicknessPercentage = this.getSicknessPercentageOnTeam(this.startDate);
			tableData.get(12).teamGroupSicknessPercentage = this.getSicknessPercentageOnTeamGroup(this.startDate);
		}
				
		((CategoryPlot)this.chart.getPlot()).setDataset(0, agentDataSet);
		
		this.table.setModel(new tableModel(tableData));
		this.table.getColumnModel().getColumn(0).setWidth(100);
	}
	
	private double getSicknessPercentageOnTeam(LocalDate fromDate){
		double sicknessMinutes = 0;
		double totalMinutes = 0;
		
		for(AbsenceAnalysisAgent a : this.data){
			if(a.getTeamCode().equals(this.currentAgent.getTeamCode())){
				for(WorkDay w : a.getWorkdays()){
					if(w.getDate().isAfter(fromDate) || w.getDate().equals(fromDate)){
						for(ExceptionInWorkDay e : w.getExceptions()){
							if(e.getType() == WorkdayExceptionType.SYG)
								if(e.getSpan() == null){
									sicknessMinutes += 60 * 7.5;
									totalMinutes += 60 * 7.5;
								}else{
									sicknessMinutes += e.getDurationInMinutes();
								}
						}
					
						if(w.getSpan() != null){
							totalMinutes += w.getSpan().toDuration().getStandardMinutes();
						}
							
					}
				}
			}
		}

		return sicknessMinutes / totalMinutes * 100;
	}
	private double getSicknessPercentageOnTeam(int month, int year){
		double sicknessMinutes = 0;
		double totalMinutes = 0;
		
		for(AbsenceAnalysisAgent a : this.data){
			if(a.getTeamCode().equals(this.currentAgent.getTeamCode())){
				for(WorkDay w : a.getWorkdays()){
					if(w.getDate().getMonthOfYear() == month && w.getDate().getYear() == year){
						for(ExceptionInWorkDay e : w.getExceptions()){
							if(e.getType() == WorkdayExceptionType.SYG)
								if(e.getSpan() == null){
									sicknessMinutes += 60 * 7.5;
									totalMinutes += 60 * 7.5;
								}else{
									sicknessMinutes += e.getDurationInMinutes();
								}
						}
					
						if(w.getSpan() != null){
							totalMinutes += w.getSpan().toDuration().getStandardMinutes();
						}
							
					}
				}
			}
		}

		return sicknessMinutes / totalMinutes * 100;
	}
	
	private double getSicknessPercentageOnTeamGroup(LocalDate fromDate){
		double sicknessMinutes = 0;
		double totalMinutes = 0;
		
		for(AbsenceAnalysisAgent a : this.data){
			if(a.getTeamGroup().equals(this.currentAgent.getTeamGroup())){
				for(WorkDay w : a.getWorkdays()){
					if(w.getDate().isAfter(fromDate) || w.getDate().equals(fromDate)){
						for(ExceptionInWorkDay e : w.getExceptions()){
							if(e.getType() == WorkdayExceptionType.SYG)
								if(e.getSpan() == null){
									sicknessMinutes += 60 * 7.5;
									totalMinutes += 60 * 7.5;
								}else{
									sicknessMinutes += e.getDurationInMinutes();
								}
						}
					
						if(w.getSpan() != null){
							totalMinutes += w.getSpan().toDuration().getStandardMinutes();
						}
							
					}
				}
			}
		}

		return sicknessMinutes / totalMinutes * 100;
	}
	private double getSicknessPercentageOnTeamGroup(int month, int year){
		double sicknessMinutes = 0;
		double totalMinutes = 0;
		
		for(AbsenceAnalysisAgent a : this.data)
			if(a.getTeamGroup().equals(this.currentAgent.getTeamGroup()))
				for(WorkDay w : a.getWorkdays()){
					if(w.getDate().getMonthOfYear() == month && w.getDate().getYear() == year){
						for(ExceptionInWorkDay e : w.getExceptions()){
							if(e.getType() == WorkdayExceptionType.SYG)
								if(e.getSpan() == null){
									sicknessMinutes += 60 * 7.5;
									totalMinutes += 60 * 7.5;
								}else
									sicknessMinutes += e.getDurationInMinutes();
						}
					
						if(w.getSpan() != null)
							totalMinutes += w.getSpan().toDuration().getStandardMinutes();
					}
				}

		
		return sicknessMinutes / totalMinutes * 100;
	}		
	
	private class tableModel extends AbstractTableModel{
		
		private static final long serialVersionUID = 1L;
		
		private ArrayList<tableData> tableData = new ArrayList<SicknessStatisticsChart.tableData>();

		public tableModel(ArrayList<tableData> tableData){
			this.tableData = tableData;
		}
		
		@Override
		public int getColumnCount() {
			return this.tableData.size() + 1;
		}

		@Override
		public int getRowCount() {
			return 3;
		}

		@Override
		public String getColumnName(int column) {
			if(SicknessStatisticsChart.this.currentAgent != null)
				if(column == 0)
					return "";
				else if(column < this.tableData.size())
					return (this.tableData.get(column - 1).monthAndYear != null ? this.tableData.get(column - 1).monthAndYear.toString("MMM") : "");
				else
					return "Total";
			else
				return "";
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			if(SicknessStatisticsChart.this.currentAgent != null)
				if(columnIndex == 0){
					if(rowIndex == 0)
						return SicknessStatisticsChart.this.currentAgent.getInitials();
					else if(rowIndex == 1)
						return SicknessStatisticsChart.this.currentAgent.getTeamCode();
					else if(rowIndex == 2)
						return SicknessStatisticsChart.this.currentAgent.getTeamGroup();
					else
						return 0;
				}else {
					if(rowIndex == 0)
						return Math.round(this.tableData.get(columnIndex - 1).agentSicknessPercentage) + "%";
					else if(rowIndex == 1)
						return Math.round(this.tableData.get(columnIndex - 1).teamSicknessPercentage) + "%";
					else if(rowIndex == 2)
						return Math.round(this.tableData.get(columnIndex - 1).teamGroupSicknessPercentage) + "%";
					else
						return 0;
				}
			else
				return "";
		}
	}
	
	private class tableData{
		public LocalDate monthAndYear;
		
		public double agentSicknessPercentage = 0;
		public double teamSicknessPercentage = 0;
		public double teamGroupSicknessPercentage = 0;
	}
}
