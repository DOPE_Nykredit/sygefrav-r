package gui;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.joda.time.LocalDate;

import absenceAnalysis.AgentSicknessAbsenceResume;
import absenceAnalysis.SicknessPeriod;

import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;
import java.awt.FlowLayout;
import java.awt.Dimension;

public class SickDaysPage extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	private final JLabel lblAgent = new JLabel();
	private final JLabel lblResponsible = new JLabel();
	private final JLabel lblDate = new JLabel();
	
	private JPanel panel = new JPanel();
	
	public void setResume(AgentSicknessAbsenceResume resume){
		this.panel.removeAll();
		this.panel.repaint();
		
		if(resume == null){
			this.lblAgent.setText(null);
			this.lblResponsible.setText("Ansvarlig: -");
						
			return;
		}
		
		this.lblAgent.setText(resume.getAgent().getInitials() + " - " + resume.getAgent().getFullName());
		this.lblResponsible.setText("Ansvarlig: " + resume.getAgent().getLeaderInitials());		
		
		if(resume.getLatestSicknessPeriod() != null){
			LocalDate insertSickessPeriodsFrom = resume.getLatestSicknessPeriod().getStart().minusMonths(3);
		
			for(SicknessPeriod s : resume.getSicknessPeriods()){
				if(s.getEnd().isAfter(insertSickessPeriodsFrom) && (resume.getAgent().getLatestNotification() == null || s.getEnd().isAfter(resume.getAgent().getLatestNotification().getCreated().toLocalDate()))){
					for(LocalDate d : s.getSickDays()){
						JLabel newDateLabel = new JLabel(d.toString("dd. MMMM - yyyy"));
						newDateLabel.setPreferredSize(new Dimension(200, 14));
						
						panel.add(newDateLabel);
					}
					
					panel.add(new JLabel(" "));
				}
			}
		}
	}
	public void setResponsible(String responsibleInitials){
		lblResponsible.setText("Ansvarlig: " + responsibleInitials);
	}
	
	public SickDaysPage() {
		setBackground(new Color(245, 245, 220));
		this.setLayout(null);
		
		panel.setBackground(new Color(245, 245, 220));
		panel.setBounds(10, 66, 112, 468);
		this.add(panel);
		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		lblAgent.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		this.lblAgent.setText("Udskrevet: " + LocalDate.now().toString("dd/MM-yyyy"));
		lblDate.setBounds(10, 35, 112, 14);
		lblDate.setVerticalAlignment(SwingConstants.TOP);
		this.add(lblDate);
				
		JLabel lblMdeAfholdt = new JLabel("M\u00F8de afholdt: _________");
		lblMdeAfholdt.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblMdeAfholdt.setBounds(400, 20, 181, 14);
		this.add(lblMdeAfholdt);
		
		lblAgent.setBounds(18, 20, 175, 14);
		add(lblAgent);
		
		lblResponsible.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblResponsible.setBounds(230, 20, 123, 15);
		add(lblResponsible);
	}
}