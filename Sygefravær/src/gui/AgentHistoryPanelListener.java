package gui;

public interface AgentHistoryPanelListener {

	void notificationSaved();
	void notificationDeleted();
	
	void exemptionSaved();
	void exemptionDeleted();
	
}
