package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JDesktopPane;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

import org.joda.time.DateTime;

import absenceAnalysis.AbsenceAnalysisAgent;
import absenceAnalysis.SicknessNotification;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CreateNotification extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	private JLabel txtAgent = new JLabel("");
	private JLabel txtCurrentLeader = new JLabel("");
	public	JComboBox cboTeamLeaders = new JComboBox();
	private JCheckBox chkPrintCalendar = new JCheckBox("Udskriv Kalender");
	private JCheckBox chkPrintGraph = new JCheckBox("Udskriv graf        ");
	
	private ArrayList<ActionListener> listerners = new ArrayList<ActionListener>();
	
	private AbsenceAnalysisAgent agent;
	
	public CreateNotification(AbsenceAnalysisAgent agent, ArrayList<String> leaderInitials){
		this.initialize();
		
		this.agent = agent;
		
		this.txtAgent.setText(agent.getInitials() + " - " + agent.getFullName());
		this.txtCurrentLeader.setText(agent.getLeaderInitials());
				
		for(String s : leaderInitials){
			this.cboTeamLeaders.addItem(s);
			
			if(agent.getLeaderInitials().equalsIgnoreCase(s))
				this.cboTeamLeaders.setSelectedItem(s);
		}
	}
	
	public boolean isPrintCalendarChecked(){
		return this.chkPrintCalendar.isSelected();
	}
	public boolean isPrintStatisticsChecked(){
		return this.chkPrintGraph.isSelected();
	}
	
	public void addActionListener(ActionListener l){
		this.listerners.add(l);
	}
	public void removeActionListener(ActionListener l){
		this.listerners.remove(l);
	}
	
	private void raiseActionEvent(String msg){
		for(ActionListener l : this.listerners)
			l.actionPerformed(new ActionEvent(this, 0, msg));
	}
	
	private void initialize(){
		setTitle("Ny notifikation");
		setName("CreateNotification");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		//setModal(true);
		setAlwaysOnTop(true);
		setBounds(100, 100, 306, 229);
		getContentPane().setLayout(new BorderLayout());
		getContentPane().setLayout(new BorderLayout());
		
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		JButton okButton = new JButton("OK");
		okButton.setPreferredSize(new Dimension(75, 23));

		okButton.setActionCommand("OK");
		okButton.addActionListener(this);
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
		JButton cancelButton = new JButton("Annuller");
		cancelButton.addActionListener(this);
		cancelButton.setPreferredSize(new Dimension(75, 23));
		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);
		
		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setOpaque(false);
		getContentPane().add(desktopPane, BorderLayout.CENTER);
		
		JLabel lblAgent = new JLabel("Agent:");
		lblAgent.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAgent.setBounds(24, 23, 73, 14);
		desktopPane.add(lblAgent);
		
		txtAgent.setBounds(107, 23, 325, 14);
		desktopPane.add(txtAgent);
		
		JLabel lblCurrentLeader = new JLabel("Leder:");
		lblCurrentLeader.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCurrentLeader.setBounds(24, 48, 73, 14);
		desktopPane.add(lblCurrentLeader);
			
		this.txtCurrentLeader.setBounds(107, 48, 325, 14);
		desktopPane.add(this.txtCurrentLeader);
		
		JLabel lblResponsible = new JLabel("Ansvarlig:");
		lblResponsible.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblResponsible.setBounds(24, 73, 73, 14);
		desktopPane.add(lblResponsible);
		
		this.cboTeamLeaders.setBounds(107, 70, 82, 20);
		desktopPane.add(this.cboTeamLeaders);
		
		this.chkPrintCalendar.setSelected(true);
		this.chkPrintCalendar.setFont(new Font("Tahoma", Font.BOLD, 11));
		this.chkPrintCalendar.setHorizontalTextPosition(SwingConstants.LEFT);
		this.chkPrintCalendar.setHorizontalAlignment(SwingConstants.LEFT);
		this.chkPrintCalendar.setBounds(21, 105, 146, 23);
		desktopPane.add(this.chkPrintCalendar);
		chkPrintGraph.setSelected(true);
		this.chkPrintGraph.setFont(new Font("Tahoma", Font.BOLD, 11));
		this.chkPrintGraph.setHorizontalAlignment(SwingConstants.LEFT);
		this.chkPrintGraph.setHorizontalTextPosition(SwingConstants.LEFT);
		this.chkPrintGraph.setBounds(24, 131, 146, 23);
		desktopPane.add(this.chkPrintGraph);		
	}
	
	private boolean validateInput(){
		if(this.cboTeamLeaders.getSelectedItem() == null){
			JOptionPane.showMessageDialog(this, 	  
					  					  "Der skete en fejl ved fors�g p� at gemme notifikation.",
					  					  "Fejl i valg",
					  					  JOptionPane.ERROR_MESSAGE);
			
			return false;
		} else {
			return true;
		}
	}
	
	public SicknessNotification getNotification(){
		if(this.validateInput())
			return new SicknessNotification(this.agent, 
											new DateTime(),
											null, 
											null,
											agent.getLeaderInitials(), 
											this.cboTeamLeaders.getSelectedItem().toString());
		else
			return null;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getActionCommand().equalsIgnoreCase("Ok"))
			if(!validateInput())
				return;
		
		this.raiseActionEvent(arg0.getActionCommand());
	}
}