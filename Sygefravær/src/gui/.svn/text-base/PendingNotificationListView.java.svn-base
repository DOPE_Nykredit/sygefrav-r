package gui;

import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;

import org.joda.time.DateTime;

import absenceAnalysis.AbsenceAnalysisAgent;
import absenceAnalysis.SicknessNotification;

public class PendingNotificationListView extends JTable {

	private static final long serialVersionUID = 1L;
	
	public enum NotificationFilter{
		OVER_5_DAYS("Over 5 dage"),
		WITHIN_1_MONTH("Indenfor 1 m�ned"),
		OVER_1_MONTH("Over 1 m�ned siden"),
		OVER_2_MONTHS("Over 2 m�neder siden"),
		ALL("Alle");
		
		private String description;
		
		private NotificationFilter(String description){
			this.description = description;
		}

		@Override
		public String toString() {
			return this.description;
		}
	}
	
	private NotificationFilter filter;
	
	private ArrayList<AbsenceAnalysisAgent> agentData = new ArrayList<AbsenceAnalysisAgent>();
	
	public void setFilter(NotificationFilter filter){
		this.filter = filter;
		
		this.updateList();
	}
	public NotificationFilter getFilter(){
		return this.filter;
	}
	
	public void setAgentData(ArrayList<AbsenceAnalysisAgent> agentData){
		if(agentData == null)
			this.agentData = new ArrayList<AbsenceAnalysisAgent>();
		else
			this.agentData = agentData;
		
		this.updateList();
	}
	public AbsenceAnalysisAgent[] getData(){
		return this.agentData.toArray(new AbsenceAnalysisAgent[this.agentData.size()]);
	}
	
	public SicknessNotification getSelectedNotification(){
		return ((PendingNotificationListViewModel)this.getModel()).getNotification(this.convertRowIndexToModel(this.getSelectedRow()));
	}
	
	public PendingNotificationListView(){
		this.setRowSelectionAllowed(true);
		this.setColumnSelectionAllowed(false);
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.setShowGrid(false);
	}
	
	public void copyContentToClipboard(){
		String content = "Oprettet\tAgent\tLeder\n\n";
			
		for(int i = 0; i < ((PendingNotificationListViewModel)this.getModel()).getRowCount(); i++){
			SicknessNotification n = ((PendingNotificationListViewModel)this.getModel()).getNotification(i);
			content += n.getCreated().toString("dd-MM-yyyy") + "\t" + n.getAgent().getInitials() + "\t" + n.getResponsibleForMeetingInitials() + "\n";
		}
		
		StringSelection s = new StringSelection(content);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(s, null);
	}
	
	
	public void updateList(){
		ArrayList<SicknessNotification> showNotifications = new ArrayList<SicknessNotification>();
	
		DateTime showFromDate;
		
		if(this.filter != null)
			switch(this.filter){
				case OVER_5_DAYS:
					showFromDate = new DateTime().minusDays(5);
				
					for(AbsenceAnalysisAgent a : this.agentData)
						for(SicknessNotification s : a.getNotifications())
							if((s.getConfirmed() == null && s.getExpired() == null) && s.getCreated().isBefore(showFromDate))
								showNotifications.add(s);		
				
					break;
				case WITHIN_1_MONTH:
					showFromDate = new DateTime().minusMonths(1);
				
					for(AbsenceAnalysisAgent a : this.agentData)
						for(SicknessNotification s : a.getNotifications())
							if((s.getConfirmed() == null && s.getExpired() == null) && s.getCreated().isAfter(showFromDate))
								showNotifications.add(s);
				
					break;
				case OVER_1_MONTH:
					showFromDate = new DateTime().minusMonths(1);
				
					for(AbsenceAnalysisAgent a : this.agentData)
						for(SicknessNotification s : a.getNotifications())
							if((s.getConfirmed() == null && s.getExpired() == null) && s.getCreated().isBefore(showFromDate))
								showNotifications.add(s);
				
					break;
				case OVER_2_MONTHS:
					showFromDate = new DateTime().minusMonths(2);
				
					for(AbsenceAnalysisAgent a : this.agentData)
						for(SicknessNotification s : a.getNotifications())
							if((s.getConfirmed() == null && s.getExpired() == null) && s.getCreated().isBefore(showFromDate))
								showNotifications.add(s);
				
					break;
				case ALL:
					for(AbsenceAnalysisAgent a : this.agentData)
						for(SicknessNotification s : a.getNotifications())
							if(s.getConfirmed() == null && s.getExpired() == null)
								showNotifications.add(s);
				
					break;
			}
		
		Collections.sort(showNotifications, new NotificationSorter());
		
		this.setModel(new PendingNotificationListViewModel(showNotifications.toArray(new SicknessNotification[showNotifications.size()])));
	}
	
	private class PendingNotificationListViewModel extends AbstractTableModel{

		private static final long serialVersionUID = 1L;
		
		private String[] columns = {"Oprettet", "Initialer", "Ansvarlig"};
		private SicknessNotification[] data;
		
		public SicknessNotification getNotification(int rowIndex){
			if(this.data.length > rowIndex && rowIndex >= 0)
				return this.data[rowIndex];
			else
				return null;
		}
		
		@Override
		public int getColumnCount() {
			return this.columns.length;
		}
		@Override
        public String getColumnName(int columnIndex) {
            return this.columns[columnIndex];
        }

		@Override
		public int getRowCount() {
			return this.data.length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			switch(columnIndex){
				case 0:
					return this.data[rowIndex].getCreated().toString("dd-MM-yyyy");
				case 1:
					return this.data[rowIndex].getAgent().getInitials();
				case 2:
					return this.data[rowIndex].getResponsibleForMeetingInitials();
				default:
					return null;
			}
		}
		
		public PendingNotificationListViewModel(SicknessNotification[] data){
			this.data = data;
		}
	}
	
	private class NotificationSorter implements java.util.Comparator<SicknessNotification> {

		@Override
		public int compare(SicknessNotification o1, SicknessNotification o2) {
			return o1.getCreated().compareTo(o2.getCreated());
		}
		
	}
}