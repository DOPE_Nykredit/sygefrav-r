package gui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;

import absenceAnalysis.AbsenceAnalysisAgent;

import com.nykredit.kundeservice.data.DopeDBException;
import com.nykredit.kundeservice.data.KSDriftConnection;
import com.nykredit.kundeservice.util.OperationAbortedException;

public class AgentListView extends JTable {

	public enum AgentFilter{
		SHOW_ALL,
		SHOW_ONLY_EXEMPTIONS
	}
	
	private static final long serialVersionUID = 1L;
	
	private ArrayList<AbsenceAnalysisAgent> agents = new ArrayList<AbsenceAnalysisAgent>();
		
	private AgentFilter filter = AgentFilter.SHOW_ALL;
	
	public AbsenceAnalysisAgent getSelectedAgent(){
		return ((AgentListViewTableModel)this.getModel()).getAgent(this.convertRowIndexToModel(this.getSelectedRow()));
	}
	
	public AgentFilter getFilter(){
		return this.filter;
	}
	public void setFilter(AgentFilter filter){	
		this.filter = filter;
		
		this.updateList();
	}
	
	public void setData(ArrayList<AbsenceAnalysisAgent> agents){
		this.agents = agents;
	}
	
	public AgentListView(){
		this.setRowSelectionAllowed(true);
		this.setColumnSelectionAllowed(false);
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}
	
	public ArrayList<AbsenceAnalysisAgent> loadAgents(boolean includeWorkForceData) throws OperationAbortedException {
		KSDriftConnection conn;
		
		try {
			conn = new KSDriftConnection();
		} catch (DopeDBException e) {
			throw new OperationAbortedException(e, "Couldnt connect to database.", "Kunne ikke oprette forbindelse til databasen.");
		}
		
		return this.loadAgents(includeWorkForceData, conn);
	}
	public ArrayList<AbsenceAnalysisAgent> loadAgents(boolean includeWorkForceData, KSDriftConnection conn) throws OperationAbortedException{
		 this.agents = AbsenceAnalysisAgent.getAllAgents(includeWorkForceData);
		 
		 Collections.sort(this.agents, new agentComparator());
		 
		 this.updateList();
		 
		 return this.agents;
	}
	
	private void updateList(){
		this.clearSelection();
		
		if (this.filter == AgentFilter.SHOW_ALL)
			this.setModel(new AgentListViewTableModel(agents));
		else {
			ArrayList<AbsenceAnalysisAgent> agentsToShow = new ArrayList<AbsenceAnalysisAgent>();
			
			for (AbsenceAnalysisAgent a : this.agents)
				if (a.hasCurrentExemption())
					agentsToShow.add(a);
			
			((AgentListViewTableModel)this.getModel()).data = agentsToShow;

		
			this.repaint();
		}
		
		this.columnModel.getColumn(0).setPreferredWidth(75);
		this.columnModel.getColumn(1).setPreferredWidth(200);
	}
	
	private class AgentListViewTableModel extends AbstractTableModel{

		private static final long serialVersionUID = 1L;

		private String[] columns = {"Initialer", "Navn", "Undtagelse"};

		private ArrayList<AbsenceAnalysisAgent> data;
		
		@Override
		public int getColumnCount() {
			return this.columns.length;
		}
        public String getColumnName(int columnIndex) {
            return this.columns[columnIndex];
        }

		@Override
		public int getRowCount() {
			return this.data.size();
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			switch (columnIndex){
			case 0:
				return this.data.get(rowIndex).getInitials();
			case 1:
				return this.data.get(rowIndex).getFullName();
			case 2:
				return this.data.get(rowIndex).hasCurrentExemption();
			default:
				return null;
			}
		}
		public AbsenceAnalysisAgent getAgent(int index){
			if(this.data.size() >= index && index >= 0)
				return this.data.get(index);
			else
				return null;
		}
		
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int columnIndex){
			return this.getValueAt(0, columnIndex).getClass();
		}
		
		public AgentListViewTableModel(ArrayList<AbsenceAnalysisAgent> agents){
			this.data = agents;
		}
	}
	
	private class agentComparator implements Comparator<AbsenceAnalysisAgent>{

		@Override
		public int compare(AbsenceAnalysisAgent o1, AbsenceAnalysisAgent o2) {
			return o1.getInitials().compareTo(o2.getInitials());
		}
	}
}
