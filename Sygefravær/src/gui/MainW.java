package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdesktop.swingx.JXDatePicker;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.LocalDate;

import absenceAnalysis.AbsenceAnalysisAgent;
import absenceAnalysis.AgentSicknessAbsenceResume;
import absenceAnalysis.SicknessNotification;

import com.nykredit.kundeservice.awt.PrintComp;
import com.nykredit.kundeservice.data.DopeDBException;
import com.nykredit.kundeservice.data.DopeException;
import com.nykredit.kundeservice.data.KSDriftConnection;
import com.nykredit.kundeservice.swing.components.CalendarPanel;
import com.nykredit.kundeservice.util.OperationAbortedException;

import gui.AgentListView.AgentFilter;
import gui.PendingNotificationListView.NotificationFilter;
import gui.ResumeListView.ResumeFilter;

public class MainW extends JFrame implements AgentHistoryPanelListener {
	
	private static final long serialVersionUID = 1L;
	
	private final JButton btnGetData = new JButton("Hent workforce data");
	
	private ResumeListView lstSicknessResumes = new ResumeListView();
	private JButton btnCreateSicknessNotificationFromResume = new JButton("Opret samtale");
	
	private final JTabbedPane tabbedPaneSicknessResumeDetails = new JTabbedPane(JTabbedPane.TOP);
	
	private final JScrollPane sicknessResumesScrollPane = new JScrollPane();
	
	private final CalendarPanel sicknessCalendarPanel = new CalendarPanel("", new LocalDate().minusMonths(12), new LocalDate());
	private final SicknessStatisticsChart sicknessStatisticsChart = new SicknessStatisticsChart();
	private SickDaysPage sickDaysPage = new SickDaysPage();
	
	private final AgentListView lstAgentHistory = new AgentListView();
	
	private final AgentHistoryPanel agentHistoryPanel = new AgentHistoryPanel();
	
	private final PendingNotificationListView lstPendingNotification = new PendingNotificationListView();
	
	private final JPanel pendingNotificationDetails = new JPanel();
	private final JLabel txtPendingNotificationAgent = new JLabel("-");
	private final JLabel txtPendingNotificationResponsible = new JLabel("-");
	private final JLabel txtPendingNotificationCurrentLeader = new JLabel("-");
	private final JCheckBox chkPendingNotificationExpired = new JCheckBox("");
	private final JButton btnPendingNotificationSave = new JButton("Gem");
	
	private final JComboBox cboStatisticsTeamLeaders = new JComboBox();
	private final JCheckBox chkStatisticsIncludeSicknessPercentage = new JCheckBox("Beregn sygefraværsprocent");
	private final JCheckBox chkStatisticsNotificationCreatedFilter = new JCheckBox("Oprettet imellem: ");
	private final JXDatePicker cboStatisticsNotificationCreatedFilterStart = new JXDatePicker();
	private final JXDatePicker cboStatisticsNotificationCreatedFilterEnd = new JXDatePicker();
	private final TeamLeaderStatisticsListView lstTeamLeaderStatistics = new TeamLeaderStatisticsListView(); 
	
	private ArrayList<AbsenceAnalysisAgent> agentData = new ArrayList<AbsenceAnalysisAgent>();
	private ArrayList<String> teamLeaderInitials = new ArrayList<String>();
			
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainW frame = new MainW();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void validate() {
		super.validate();
		
		this.tabbedPaneSicknessResumeDetails.setSize(this.getWidth() - 326, this.getHeight() - 125);
	}

	public MainW() {
		this.setTitle("Sygefrav\u00E6rssamtaler");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(200, 50, 972, 692);
		
		JTabbedPane tabbedPaneMain = new JTabbedPane(JTabbedPane.TOP);
		this.getContentPane().add(tabbedPaneMain, BorderLayout.CENTER);
		
		JDesktopPane panelSicknessResumes = new JDesktopPane();
		panelSicknessResumes.setOpaque(false);
		tabbedPaneMain.addTab("<html><body leftmargin=30 topmargin=3 marginwidth=25 marginheight=2>Check sygeperioder</body></html>", null, panelSicknessResumes, null);

		this.btnGetData.setBounds(10, 11, 148, 23);
		this.btnGetData.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.loadWorkForceData();
			}
		});
		panelSicknessResumes.add(this.btnGetData);
				
		this.lstSicknessResumes.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(e.getValueIsAdjusting() == false)
					MainW.this.currentResumeSelectionChanged(MainW.this.lstSicknessResumes.getSelectedResume());
			}
		});
		
		this.sicknessResumesScrollPane.setBounds(10, 45, 280, 505);
		this.sicknessResumesScrollPane.setViewportView(this.lstSicknessResumes);
		panelSicknessResumes.add(this.sicknessResumesScrollPane);
		
		JRadioButton rdbtnShowAllResumes = new JRadioButton("Vis alle");
		rdbtnShowAllResumes.setOpaque(false);
		rdbtnShowAllResumes.setBounds(10, 557, 57, 23);
		rdbtnShowAllResumes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.lstSicknessResumes.setFilter(ResumeFilter.SHOW_ALL);
			}
		});
		panelSicknessResumes.add(rdbtnShowAllResumes);
		
		JRadioButton rdbtnShowResumesDueForNotification = new JRadioButton("Vis overskredne");
		rdbtnShowResumesDueForNotification.setSelected(true);
		rdbtnShowResumesDueForNotification.setOpaque(false);
		rdbtnShowResumesDueForNotification.setBounds(105, 557, 109, 23);
		rdbtnShowResumesDueForNotification.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.lstSicknessResumes.setFilter(ResumeFilter.SHOW_PERIODLIMIT_EXCEEDED);
			}
		});
		panelSicknessResumes.add(rdbtnShowResumesDueForNotification);
		
		ButtonGroup resumeListFilter = new ButtonGroup();
		resumeListFilter.add(rdbtnShowAllResumes);
		resumeListFilter.add(rdbtnShowResumesDueForNotification);
		this.btnCreateSicknessNotificationFromResume.setEnabled(false);
		
		this.btnCreateSicknessNotificationFromResume.setBounds(10, 587, 100, 23);
		this.btnCreateSicknessNotificationFromResume.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.btnCreateSicknessNotificationFromResume_Clicked();
			}
		});
		panelSicknessResumes.add(this.btnCreateSicknessNotificationFromResume);
		sicknessCalendarPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
		sicknessCalendarPanel.setBackground(Color.WHITE);
		
		JScrollPane scrollPaneSicknessCalendar = new JScrollPane(this.sicknessCalendarPanel);
		scrollPaneSicknessCalendar.setBorder(null);
		
		this.tabbedPaneSicknessResumeDetails.addTab("Kalender", null, scrollPaneSicknessCalendar, null);
		sicknessStatisticsChart.setBackground(Color.WHITE);
		
		sicknessStatisticsChart.setSize(new Dimension(200, 100));
		sicknessStatisticsChart.setMinimumSize(new Dimension(200, 100));
		
		this.sicknessStatisticsChart.setStartDateMonth(LocalDate.now().minusMonths(12).getMonthOfYear());
		this.sicknessStatisticsChart.setStartDateYear(LocalDate.now().minusYears(1).getYear());
		JScrollPane scrollPaneSicknessGraph = new JScrollPane(this.sicknessStatisticsChart);
		scrollPaneSicknessGraph.setBorder(null);
		
		this.tabbedPaneSicknessResumeDetails.addTab("Graf", null, scrollPaneSicknessGraph, null);
		this.tabbedPaneSicknessResumeDetails.setBounds(300, 43, 646, 567);
		panelSicknessResumes.add(this.tabbedPaneSicknessResumeDetails);
		
		JScrollPane scrollPaneSicknessDetails = new JScrollPane();
		scrollPaneSicknessDetails.setViewportView(this.sickDaysPage);
		tabbedPaneSicknessResumeDetails.addTab("Forside", null, scrollPaneSicknessDetails, null);
				
		JDesktopPane desktopPaneHistory = new JDesktopPane();
		desktopPaneHistory.setOpaque(false);
				
		tabbedPaneMain.addTab("<html><body leftmargin=30 topmargin=3 marginwidth=25 marginheight=2>Historik</body></html>", null, desktopPaneHistory, null);
		
		this.lstAgentHistory.setShowHorizontalLines(false);
		this.lstAgentHistory.setFillsViewportHeight(true);
		this.lstAgentHistory.setShowGrid(false);
		this.lstAgentHistory.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting() == false)
					MainW.this.agentHistoryPanel.setAgent(MainW.this.lstAgentHistory.getSelectedAgent());
			}
		});
		
		JScrollPane agentListScrollPane = new JScrollPane(this.lstAgentHistory);
		agentListScrollPane.setBounds(10, 45, 329, 421);
		desktopPaneHistory.add(agentListScrollPane);
		
		JRadioButton rdbtnShowAll = new JRadioButton("Vis alle");
		rdbtnShowAll.setSelected(true);
		rdbtnShowAll.setBounds(10, 473, 66, 23);
		rdbtnShowAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.lstAgentHistory.setFilter(AgentFilter.SHOW_ALL);		
			}
		});
		rdbtnShowAll.setOpaque(false);
		desktopPaneHistory.add(rdbtnShowAll);
		
		JRadioButton rdbtnShowOnlyExemptions = new JRadioButton("Vis kun undtagelser");
		rdbtnShowOnlyExemptions.setBounds(91, 473, 119, 23);
		rdbtnShowOnlyExemptions.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.lstAgentHistory.setFilter(AgentFilter.SHOW_ONLY_EXEMPTIONS);
			}
		});
		rdbtnShowOnlyExemptions.setOpaque(false);
		desktopPaneHistory.add(rdbtnShowOnlyExemptions);
		
		ButtonGroup agentListFilterOptions = new ButtonGroup();
		agentListFilterOptions.add(rdbtnShowAll);
		agentListFilterOptions.add(rdbtnShowOnlyExemptions);

		this.agentHistoryPanel.setBounds(363, 45, 426, 412);
		desktopPaneHistory.add(this.agentHistoryPanel);
		
		JDesktopPane desktopPaneReview = new JDesktopPane();
		desktopPaneReview.setOpaque(false);
		
		tabbedPaneMain.addTab("<html><body leftmargin=30 topmargin=3 marginwidth=25 marginheight=2>Opf\u00F8lgning</body></html>", null, desktopPaneReview, null);
		
		this.lstPendingNotification.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				MainW.this.currentPendingNotificationChanged();		
			}
		});
		JScrollPane pendingNotificationsScrollPane = new JScrollPane(this.lstPendingNotification);
		pendingNotificationsScrollPane.setBounds(10, 45, 280, 505);
		desktopPaneReview.add(pendingNotificationsScrollPane);
		
		JButton btnCopyPendingNotifications = new JButton("Kopier liste");
		btnCopyPendingNotifications.setBounds(10, 561, 89, 23);
		btnCopyPendingNotifications.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				MainW.this.lstPendingNotification.copyContentToClipboard();
			}
		});
		desktopPaneReview.add(btnCopyPendingNotifications);
		
		JLabel lblPendingNotificationFilter = new JLabel("Vis notifikationer:");
		lblPendingNotificationFilter.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPendingNotificationFilter.setBounds(10, 17, 120, 14);
		desktopPaneReview.add(lblPendingNotificationFilter);
		
		JComboBox cboPendingNotificationFilter = new JComboBox(PendingNotificationListView.NotificationFilter.values());
		cboPendingNotificationFilter.setBounds(153, 14, 137, 20);
		cboPendingNotificationFilter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.lstPendingNotification.setFilter((PendingNotificationListView.NotificationFilter)((JComboBox)e.getSource()).getSelectedItem());
			}
		});
		desktopPaneReview.add(cboPendingNotificationFilter);
		
		this.pendingNotificationDetails.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Afventende samtale", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 70, 213)));
		this.pendingNotificationDetails.setBackground(new Color(0, 0, 0));
		this.pendingNotificationDetails.setOpaque(false);
		this.pendingNotificationDetails.setBounds(310, 47, 371, 187);
		this.pendingNotificationDetails.setLayout(null);
		desktopPaneReview.add(this.pendingNotificationDetails);
				
		JLabel lblPendingNotificationAgent = new JLabel("Agent:");
		lblPendingNotificationAgent.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPendingNotificationAgent.setBounds(28, 31, 76, 14);
		this.pendingNotificationDetails.add(lblPendingNotificationAgent);
		
		JLabel lblPendingNotificationResponsible = new JLabel("Ansvarlig:");
		lblPendingNotificationResponsible.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPendingNotificationResponsible.setBounds(28, 61, 76, 14);
		this.pendingNotificationDetails.add(lblPendingNotificationResponsible);
		
		JLabel lblPendingNotificationCurrentLeader = new JLabel("Leder:");
		lblPendingNotificationCurrentLeader.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPendingNotificationCurrentLeader.setBounds(28, 91, 76, 14);
		pendingNotificationDetails.add(lblPendingNotificationCurrentLeader);
		
		txtPendingNotificationAgent.setBounds(114, 31, 206, 14);
		pendingNotificationDetails.add(txtPendingNotificationAgent);
		
		this.txtPendingNotificationCurrentLeader.setBounds(114, 91, 206, 14);
		pendingNotificationDetails.add(txtPendingNotificationCurrentLeader);
		
		txtPendingNotificationResponsible.setBounds(114, 61, 206, 14);
		this.pendingNotificationDetails.add(txtPendingNotificationResponsible);
		
		JLabel lblPendingNotificationExpired = new JLabel("Ikke afholdt:");
		lblPendingNotificationExpired.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPendingNotificationExpired.setBounds(28, 121, 76, 14);
		this.pendingNotificationDetails.add(lblPendingNotificationExpired);
		chkPendingNotificationExpired.setEnabled(false);
		
		this.chkPendingNotificationExpired.setOpaque(false);
		this.chkPendingNotificationExpired.setBounds(114, 117, 21, 23);
		this.pendingNotificationDetails.add(chkPendingNotificationExpired);
		btnPendingNotificationSave.setEnabled(false);
		
		this.btnPendingNotificationSave.setBounds(260, 150, 89, 23);
		this.btnPendingNotificationSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.savePendingNotification();
			}
		});
		pendingNotificationDetails.add(btnPendingNotificationSave);
				
		JDesktopPane desktopPaneStatistics = new JDesktopPane();
		desktopPaneStatistics.setOpaque(false);
		
		tabbedPaneMain.addTab("<html><body leftmargin=30 topmargin=3 marginwidth=25 marginheight=2>Statistik</body></html>", null, desktopPaneStatistics, null);
		
		JLabel lblstatisticTeamLeaderFilter = new JLabel("Ansvarlig for samtaler:");
		lblstatisticTeamLeaderFilter.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblstatisticTeamLeaderFilter.setBounds(10, 17, 130, 14);
		desktopPaneStatistics.add(lblstatisticTeamLeaderFilter);
		
		cboStatisticsTeamLeaders.setBounds(150, 14, 80, 20);
		cboStatisticsTeamLeaders.setMaximumRowCount(12);
		cboStatisticsTeamLeaders.addItem("Alle");
		desktopPaneStatistics.add(cboStatisticsTeamLeaders);
		
		chkStatisticsIncludeSicknessPercentage.setBounds(260, 18, 170, 14);
		chkStatisticsIncludeSicknessPercentage.setOpaque(false);
		desktopPaneStatistics.add(chkStatisticsIncludeSicknessPercentage);
		
		JButton btnLoadStatistics = new JButton("Hent data");
		btnLoadStatistics.setBounds(440, 15, 100, 22);
		btnLoadStatistics.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainW.this.updateTeamLeadersStatistics();
			}
		});
		desktopPaneStatistics.add(btnLoadStatistics);		
		
		chkStatisticsNotificationCreatedFilter.setBounds(15, 55, 115, 14);
		chkStatisticsNotificationCreatedFilter.setOpaque(false);
		chkStatisticsNotificationCreatedFilter.setSelected(true);
		chkStatisticsNotificationCreatedFilter.setFont(new Font("Tahoma", Font.PLAIN, 11));
		chkStatisticsNotificationCreatedFilter.addActionListener(new ActionListener() {
			Date selectedStartDate = null;
			Date selectedEndDate = null;
			
			public void actionPerformed(ActionEvent e) {				
				JCheckBox source = (JCheckBox)e.getSource();
				
				boolean checked = source.isSelected();
							
				MainW.this.cboStatisticsNotificationCreatedFilterStart.setEnabled(checked);
				MainW.this.cboStatisticsNotificationCreatedFilterEnd.setEnabled(checked);
				
				if(checked){
					MainW.this.cboStatisticsNotificationCreatedFilterStart.setDate(selectedStartDate);
					MainW.this.cboStatisticsNotificationCreatedFilterEnd.setDate(selectedEndDate);
				}else{
					selectedStartDate = MainW.this.cboStatisticsNotificationCreatedFilterStart.getDate();
					selectedEndDate = MainW.this.cboStatisticsNotificationCreatedFilterEnd.getDate();
					
					MainW.this.cboStatisticsNotificationCreatedFilterStart.setDate(null);
					MainW.this.cboStatisticsNotificationCreatedFilterEnd.setDate(null);
				}				
			}
		});
		desktopPaneStatistics.add(chkStatisticsNotificationCreatedFilter);
		
		Calendar notificationFilterStart = Calendar.getInstance();
		notificationFilterStart.add(Calendar.MONTH, -1);
		
		this.cboStatisticsNotificationCreatedFilterStart.setBounds(130, 52, 100, 20);
		this.cboStatisticsNotificationCreatedFilterStart.getEditor().setEditable(false);
		this.cboStatisticsNotificationCreatedFilterStart.setFormats(new SimpleDateFormat("dd-MM-yyyy"));
		this.cboStatisticsNotificationCreatedFilterStart.setBorder(null);
		this.cboStatisticsNotificationCreatedFilterStart.setDate(notificationFilterStart.getTime());
		this.cboStatisticsNotificationCreatedFilterStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JXDatePicker source = (JXDatePicker)e.getSource();
				
				if(source.getDate() != null)
					MainW.this.cboStatisticsNotificationCreatedFilterEnd.getMonthView().setLowerBound(source.getDate());
				else if(MainW.this.chkStatisticsNotificationCreatedFilter.isSelected())
					source.setDate(MainW.this.cboStatisticsNotificationCreatedFilterEnd.getMonthView().getUpperBound());
			}
		});
		desktopPaneStatistics.add(this.cboStatisticsNotificationCreatedFilterStart);
		
		JLabel lblStatisticsNotificationCreatedFilter = new JLabel("og");
		lblStatisticsNotificationCreatedFilter.setBounds(242, 54, 15, 14);
		desktopPaneStatistics.add(lblStatisticsNotificationCreatedFilter);
		
		this.cboStatisticsNotificationCreatedFilterEnd.setBounds(265, 52, 100, 20);
		this.cboStatisticsNotificationCreatedFilterEnd.getEditor().setEditable(false);
		this.cboStatisticsNotificationCreatedFilterEnd.setFormats(new SimpleDateFormat("dd-MM-yyyy"));
		this.cboStatisticsNotificationCreatedFilterEnd.setBorder(null);
		this.cboStatisticsNotificationCreatedFilterEnd.setDate(new Date());
		this.cboStatisticsNotificationCreatedFilterEnd.getMonthView().setUpperBound(new Date());
		this.cboStatisticsNotificationCreatedFilterEnd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				JXDatePicker source = (JXDatePicker)e.getSource();
				
				if(source.getDate() != null)
					MainW.this.cboStatisticsNotificationCreatedFilterStart.getMonthView().setUpperBound(source.getDate());
				else if(MainW.this.chkStatisticsNotificationCreatedFilter.isSelected())
					source.setDate(MainW.this.cboStatisticsNotificationCreatedFilterEnd.getMonthView().getLowerBound());
			}
		});
		desktopPaneStatistics.add(this.cboStatisticsNotificationCreatedFilterEnd);
		
		this.cboStatisticsNotificationCreatedFilterStart.getMonthView().setUpperBound(this.cboStatisticsNotificationCreatedFilterEnd.getDate());
		this.cboStatisticsNotificationCreatedFilterEnd.getMonthView().setLowerBound(this.cboStatisticsNotificationCreatedFilterStart.getDate());
		
		JScrollPane teamLeaderStatisticsScrollPane = new JScrollPane(this.lstTeamLeaderStatistics);
		teamLeaderStatisticsScrollPane.setBounds(10, 90, 400, 488);
		desktopPaneStatistics.add(teamLeaderStatisticsScrollPane);
		
		JButton btnCopyStatistics = new JButton("Kopier liste");
		btnCopyStatistics.setBounds(10, 592, 89, 23);
		btnCopyStatistics.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.lstTeamLeaderStatistics.copyContentToClipboard();
			}
		});
		desktopPaneStatistics.add(btnCopyStatistics);
		
		try {
			this.teamLeaderInitials = AbsenceAnalysisAgent.retrieveLeaderInitials();
			
			this.agentHistoryPanel.setTeamLeaderInitials(this.teamLeaderInitials);

			for(String s : this.teamLeaderInitials)
				this.cboStatisticsTeamLeaders.addItem(s);
		} catch (OperationAbortedException e) {
			JOptionPane.showMessageDialog(this, "Kunne ikke indlæse teamchefer. Det anbefales at du genstarter programmet. /n /n Detaljer: " + e.getDopeErrorMsg(), "Fejl.", JOptionPane.ERROR_MESSAGE);
		}
		
		try {
			KSDriftConnection conn = new KSDriftConnection();
			
			SicknessNotification.getNotifications(conn, null, null, "NEDA");
			
			this.sicknessCalendarPanel.LoadKundeserviceBusinessDates(conn);
			this.agentData = this.lstAgentHistory.loadAgents(false, conn);
			
			this.lstPendingNotification.setAgentData(this.agentData);
			this.lstTeamLeaderStatistics.setAgentData(this.agentData);
			
			conn.closeConnection();
		} catch (DopeException e) {
			JOptionPane.showMessageDialog(this, "Kunne ikke indlæse agenter. Det anbefales at du genstarter programmet. /n /n Detaljer: " + e.getDopeErrorMsg(), "Fejl.", JOptionPane.ERROR_MESSAGE);
		}
		
		cboPendingNotificationFilter.setSelectedIndex(2);
	}
	
	private void loadWorkForceData(){
		this.agentData = new ArrayList<AbsenceAnalysisAgent>();
		
		try {
			this.agentData = this.lstAgentHistory.loadAgents(true);
		} catch (OperationAbortedException ex) {
			JOptionPane.showMessageDialog(MainW.this, 
										  "Der skete en fejl ved indlæsning af workforcedata. /n /n Detaljer: " + ex.getDopeErrorMsg(), 
										  "Fejl", 
										  JOptionPane.ERROR_MESSAGE);
		}
						
		ArrayList<AgentSicknessAbsenceResume> resumes = new ArrayList<AgentSicknessAbsenceResume>();
		
		for(AbsenceAnalysisAgent a : this.agentData)
			resumes.add(a.getSicknessResume());
				
		this.lstSicknessResumes.setData(resumes);
		
		this.sicknessStatisticsChart.setData(this.agentData);
		this.lstPendingNotification.setAgentData(this.agentData);
	}
		
	private void btnCreateSicknessNotificationFromResume_Clicked(){
		if(this.lstSicknessResumes.getSelectedResume() != null){
			AgentSicknessAbsenceResume selectedResume = this.lstSicknessResumes.getSelectedResume();
			
			CreateNotification notificationDialog = new CreateNotification(selectedResume.getAgent(), this.teamLeaderInitials);
			notificationDialog.addActionListener(new createNotificationListener());			
			
			notificationDialog.setVisible(true);
		}
	}

	private void savePendingNotification(){
		if (this.chkPendingNotificationExpired.isSelected()){
			SicknessNotification pendingNotification = this.lstPendingNotification.getSelectedNotification();
		
			pendingNotification.setExpired(new DateTime());
			try {
				pendingNotification.save();
			} catch (DopeDBException e) {
				JOptionPane.showMessageDialog(MainW.this, 
	  					  					  "Der skete en fejl ved opdatering af notifikation. Det anbefales at du lukker programmet ned og starter forfra. \n \n Detaljer: " + e.getDopeErrorMsg(), 
	  					  					  "Fejl", 
	  					  					  JOptionPane.ERROR_MESSAGE);
			}
			
			this.lstPendingNotification.updateList();
		}
	}
	
	private void currentResumeSelectionChanged(AgentSicknessAbsenceResume resume){
		this.sicknessCalendarPanel.clearCalendarMarkings();
				
		if(resume != null){
			this.sicknessCalendarPanel.addCalendarMarking("Sickness", this.sicknessCalendarPanel.new CalendarMarkings("Sygdom", resume.getSickDays(), Color.YELLOW));
			this.sicknessCalendarPanel.addCalendarMarking("DaysOff", this.sicknessCalendarPanel.new CalendarMarkings("Fri", resume.getDaysOff(), new Color(200, 200, 200)));
			
			this.sicknessCalendarPanel.setTitle("Sygekalender: " + resume.getAgent().getInitials() + " - " + resume.getAgent().getFullName());
		
			this.sicknessStatisticsChart.setCurrentAgent(resume.getAgent());
			
			this.sickDaysPage.setResume(resume);
			
			this.btnCreateSicknessNotificationFromResume.setEnabled(true);
		}else{
			this.sicknessCalendarPanel.setTitle(null);
			this.sicknessStatisticsChart.setCurrentAgent(null);
			this.btnCreateSicknessNotificationFromResume.setEnabled(false);
			
			this.sickDaysPage.setResume(null);
		}
		
		this.sicknessCalendarPanel.updateMarkings();
		this.sicknessStatisticsChart.updateChart();
	}
	
	private void currentPendingNotificationChanged(){
		SicknessNotification currentPendingNotification = MainW.this.lstPendingNotification.getSelectedNotification();
		
		if (currentPendingNotification != null){	
			this.txtPendingNotificationAgent.setText(currentPendingNotification.getAgent().getInitials() + " - " + currentPendingNotification.getAgent().getFullName());
			this.txtPendingNotificationResponsible.setText(currentPendingNotification.getResponsibleForMeetingInitials());
			this.txtPendingNotificationCurrentLeader.setText(currentPendingNotification.getLeaderInitials());
			
			this.chkPendingNotificationExpired.setEnabled(true);
			this.btnPendingNotificationSave.setEnabled(true);
		} else {
			this.txtPendingNotificationAgent.setText("-");
			this.txtPendingNotificationResponsible.setText("-");
			this.txtPendingNotificationCurrentLeader.setText("-");
			
			this.chkPendingNotificationExpired.setEnabled(false);
			this.btnPendingNotificationSave.setEnabled(false);
		}
		
		this.chkPendingNotificationExpired.setSelected(false);
	}

	private void updateTeamLeadersStatistics(){
		String responsibleInitials = null;
		Interval createdSpan = null;
		boolean includeSicknessPercentage;
		
		if(!this.cboStatisticsTeamLeaders.getSelectedItem().equals("Alle"))
			responsibleInitials = this.cboStatisticsTeamLeaders.getSelectedItem().toString();
		
		if(this.chkStatisticsNotificationCreatedFilter.isSelected()){
			DateTime start = new DateTime(this.cboStatisticsNotificationCreatedFilterStart.getDate());
			DateTime end = new DateTime(this.cboStatisticsNotificationCreatedFilterEnd.getDate());
			
			createdSpan = new Interval(start, end);
		}
		
		includeSicknessPercentage = this.chkStatisticsIncludeSicknessPercentage.isSelected();
		
		try {
			this.lstTeamLeaderStatistics.updateList(responsibleInitials, 
													createdSpan, 
													includeSicknessPercentage);
		} catch (DopeException e) {
			JOptionPane.showMessageDialog(this, 
										  e.getDopeErrorMsg() + "\n\n Kan ikke beregne sygefraværsprocent.", 
					  					  "Fejl",
					  					  JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private class createNotificationListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			CreateNotification notificationDialog = (CreateNotification)e.getSource();
			
			if (e.getActionCommand().equalsIgnoreCase("Ok")){
				SicknessNotification notification = notificationDialog.getNotification();

				notificationDialog.setVisible(false);
				notificationDialog.dispose();
				
				try {
					notification.save();
					AbsenceAnalysisAgent selectedAgent = MainW.this.lstAgentHistory.getSelectedAgent();
					
					if(selectedAgent != null && selectedAgent.equals(notification.getAgent()))
						MainW.this.agentHistoryPanel.setAgent(selectedAgent);
						
						//MainW.this.lstAgentNotifications.setListData(selectedAgent.getNotifications().toArray());
					
					ArrayList<JComponent> componentsToPrint = new ArrayList<JComponent>();
					
					MainW.this.sickDaysPage.setResponsible(notification.getResponsibleForMeetingInitials());
					MainW.this.sickDaysPage.validate();
					componentsToPrint.add(MainW.this.sickDaysPage);

					if(notificationDialog.isPrintCalendarChecked()){
						MainW.this.sicknessCalendarPanel.setTitleVisible(true);
						MainW.this.sicknessCalendarPanel.validate();
						componentsToPrint.add(MainW.this.sicknessCalendarPanel);
					}
					
					if(notificationDialog.isPrintStatisticsChecked()){
						MainW.this.sicknessStatisticsChart.validate();
						componentsToPrint.add(MainW.this.sicknessStatisticsChart);
					}
					
					PrintComp.printComponents(componentsToPrint, "Sygefravær - " + notification.getAgent().getInitials());
					
					MainW.this.sicknessCalendarPanel.setTitleVisible(false);
					
					MainW.this.lstSicknessResumes.updateList();
					
					if(MainW.this.lstPendingNotification.getFilter() == NotificationFilter.WITHIN_1_MONTH || MainW.this.lstPendingNotification.getFilter() == NotificationFilter.ALL)
						MainW.this.lstPendingNotification.updateList();
					
				} catch (DopeDBException ex) {
					JOptionPane.showMessageDialog(MainW.this, 
		  					  					  "Der skete en fejl ved oprettelse af ny notifikation. Det anbefales at du lukker programmet ned og starter forfra. \n \nDetaljer: " + ex.getDopeErrorMsg(), 
		  					  					  "Fejl", 
		  					  					  JOptionPane.ERROR_MESSAGE);
				}
			}else if(e.getActionCommand().equalsIgnoreCase("Cancel")){
				notificationDialog.dispose();
			}
		}
	}

	
	@Override
	public void notificationSaved() {
		this.lstSicknessResumes.updateList();
		this.lstPendingNotification.updateList();
	}

	@Override
	public void notificationDeleted() {
		this.lstSicknessResumes.updateList();
		this.lstPendingNotification.updateList();		
	}

	@Override
	public void exemptionSaved() {
		this.lstAgentHistory.updateUI();
		this.lstSicknessResumes.updateList();
	}

	@Override
	public void exemptionDeleted() {
		this.lstAgentHistory.updateUI();
		this.lstSicknessResumes.updateList();		
	}
}