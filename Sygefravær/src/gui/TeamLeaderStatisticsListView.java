package gui;

import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import com.nykredit.kundeservice.data.CTIWF2Connection;
import com.nykredit.kundeservice.data.DopeDBException;
import com.nykredit.kundeservice.util.OperationAbortedException;
import com.nykredit.kundeservice.workforce.AgentSchedule;
import com.nykredit.kundeservice.workforce.WorkForceData;

import absenceAnalysis.AbsenceAnalysisAgent;
import absenceAnalysis.SicknessNotification;

public class TeamLeaderStatisticsListView extends JTable {
	
	private static final long serialVersionUID = 1L;
		
	private ArrayList<AbsenceAnalysisAgent> agentData;

	public void setAgentData(ArrayList<AbsenceAnalysisAgent> agentData){
		this.agentData = agentData;
	}
	
	public SicknessNotification getSelectedNotification(){
		return ((TeamLeaderStatisticsListViewModel)this.getModel()).getNotification(this.convertRowIndexToModel(this.getSelectedRow()));
	}
	
	public TeamLeaderStatisticsListView(){
		this.setRowSelectionAllowed(true);
		this.setColumnSelectionAllowed(false);
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.setShowGrid(false);
	}
	
	public void copyContentToClipboard(){
		String content = "";
			
		for(int i = 0; i < this.getModel().getColumnCount(); i++)
			content += this.getColumnName(i) + "\t";
		
		content += "\n\n";
		
		for(int i = 0; i < ((TeamLeaderStatisticsListViewModel)this.getModel()).getRowCount(); i++){
			TableModel model = this.getModel();
			
			for(int j = 0; j < this.getModel().getColumnCount(); j++)
				content += model.getValueAt(i, j) + (j > 0 ? "\t\t" : "\t");
			
			content += "\n";
			
			//content += n.getCreated().toString("dd-MM-yyyy") + "\t" + n.getAgent().getInitials() + "\t" + n.getResponsibleForMeetingInitials() + "\t" + n "\n";
		}
		
		StringSelection s = new StringSelection(content);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(s, null);
	}
	
	
	public void updateList(String responsibleInitials, Interval createdSpan, boolean includeSicknessPercentage) throws OperationAbortedException, DopeDBException{
		ArrayList<SicknessNotification> showNotifications = new ArrayList<SicknessNotification>();
	
		if(this.agentData != null)
			for(AbsenceAnalysisAgent a : this.agentData)
				for(SicknessNotification n : a.getNotifications()){
					boolean includeNotification = true;
					
					if(responsibleInitials != null && !n.getResponsibleForMeetingInitials().equalsIgnoreCase(responsibleInitials))
						includeNotification = false;
					
					if(createdSpan != null && !createdSpan.contains(n.getCreated()))
						includeNotification = false;
					
					if(includeNotification)
						showNotifications.add(n);
				}
		
		Collections.sort(showNotifications, new NotificationSorter());
		
		this.setModel(new TeamLeaderStatisticsListViewModel(showNotifications.toArray(new SicknessNotification[0]), includeSicknessPercentage));
		
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
				
		this.getColumnModel().getColumn(0).setPreferredWidth(35);
		
		this.getColumnModel().getColumn(1).setPreferredWidth(40);
		this.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
		
		this.getColumnModel().getColumn(2).setPreferredWidth(50);
		this.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
		
		this.getColumnModel().getColumn(3).setPreferredWidth(30);
		this.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
		
		this.getColumnModel().getColumn(4).setPreferredWidth(30);
		this.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
	}
	
	private class TeamLeaderStatisticsListViewModel extends AbstractTableModel{

		private static final long serialVersionUID = 1L;
		
		private String[] columns = {"Oprettet", 
									"Initialer", 
									"Ansvarlig", 
									"Dage �ben",
									"Frav�rs %"};
		private SicknessNotification[] notificationData;
		private String[] sicknessPercentages;
		
		public SicknessNotification getNotification(int rowIndex){
			if(this.notificationData.length > rowIndex && rowIndex >= 0)
				return this.notificationData[rowIndex];
			else
				return null;
		}
		
		@Override
		public int getColumnCount() {
			return this.columns.length;
		}
		@Override
        public String getColumnName(int columnIndex) {
            return this.columns[columnIndex];
        }

		@Override
		public int getRowCount() {
			return this.notificationData.length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			switch(columnIndex){
				case 0:
					return this.notificationData[rowIndex].getCreated().toString("dd/MM-yyyy");
				case 1:
					return this.notificationData[rowIndex].getAgent().getInitials();
				case 2:
					return this.notificationData[rowIndex].getResponsibleForMeetingInitials();
				case 3:
					DateTime calculateFrom;
					DateTime calculateTo;

					calculateFrom = this.notificationData[rowIndex].getCreated();
					
					if(this.notificationData[rowIndex].getConfirmed() != null)
						calculateTo = this.notificationData[rowIndex].getConfirmed();
					else if(this.notificationData[rowIndex].getExpired() != null)
						calculateTo = this.notificationData[rowIndex].getExpired();
					else
						return "-";

					Interval interval = new Interval(calculateFrom, calculateTo);
					
					return interval.toDuration().getStandardDays();
				case 4:
					if(this.sicknessPercentages != null)
						return this.sicknessPercentages[rowIndex];
				default:
					return null;
			}
		}
		
		public TeamLeaderStatisticsListViewModel(SicknessNotification[] data, boolean showSicknessPercentage) throws OperationAbortedException, DopeDBException{
			this.notificationData = data;
			
			if(showSicknessPercentage){
				this.sicknessPercentages = new String[data.length];
				
				CTIWF2Connection conn = new CTIWF2Connection();
				
				for(int i = 0; i < data.length; i++){
					SicknessNotification notification = data[i];
					
					AgentSchedule yearSchedule = WorkForceData.getAgentSchedule(notification.getAgent(), 
																				new Interval(notification.getCreated().minusYears(1),
																							 notification.getCreated()), 
																				conn);
					
					AbsenceAnalysisAgent agent = new AbsenceAnalysisAgent(yearSchedule, 
																		  notification.getAgent().getTeamCode(), 
																		  notification.getAgent().getTeamGroup(), 
																		  notification.getAgent().getLeaderInitials());
					
					double sicknessPercentage = agent.getSicknessPercentage(notification.getCreated().minusYears(1).toLocalDate(),
																			notification.getCreated().toLocalDate());
									
					BigDecimal bd = new BigDecimal(sicknessPercentage);
					bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
										
					sicknessPercentages[i] = String.valueOf(bd.doubleValue()) + "%"; 
				}
			}
				
		}
	}
	
	private class NotificationSorter implements java.util.Comparator<SicknessNotification> {
		public int compare(SicknessNotification o1, SicknessNotification o2) {
			return o1.getCreated().compareTo(o2.getCreated());
		}
	}
}