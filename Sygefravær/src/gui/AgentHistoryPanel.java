package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdesktop.swingx.JXDatePicker;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.nykredit.kundeservice.data.DopeDBException;

import absenceAnalysis.AbsenceAnalysisAgent;
import absenceAnalysis.ExemptionFromAnalysis;
import absenceAnalysis.SicknessNotification;

public class AgentHistoryPanel extends JDesktopPane {
	
	private static final long serialVersionUID = 1L;
	
	private ArrayList<String> teamLeaderInitials = new ArrayList<String>(); 
	private AbsenceAnalysisAgent selectedAgent;
	
	private ArrayList<AgentHistoryPanelListener> listeners = new ArrayList<AgentHistoryPanelListener>();
	
	private final JLabel lblSelectedAgentInitials = new JLabel("-");
	private final JLabel lblSelectedAgentName = new JLabel("-");
	private final JLabel lblSelectedAgentTeam = new JLabel("-");
	private final JLabel lblSelectedAgentLeader = new JLabel("-");
	
	private final JComboBox cboNotificationResponsible = new JComboBox();
	private final JXDatePicker datePickerNotificationCreated = new JXDatePicker();
	private final JXDatePicker datePickerNotificationConfirmed = new JXDatePicker();
	private final JXDatePicker datePickerNotificationExpired = new JXDatePicker();
	private final JLabel txtSelectedNotificationLeader = new JLabel("-");
	private final JButton btnDeleteNotification = new JButton("Slet valgte");
	private final JButton btnNewNotification = new JButton("Opret ny");
	private final JButton btnSaveNotification = new JButton("Gem");
	
	private final JList lstAgentExemptions = new JList();
	private final JXDatePicker datePickerExemptionStart = new JXDatePicker();
	private final JXDatePicker datePickerExemptionEnd = new JXDatePicker();
	private final JTextArea txtExemptionNote = new JTextArea();
	private final JButton btnDeleteExemption = new JButton("Slet valgte");
	private final JButton btnNewExemption = new JButton("Opret ny");
	private final JButton btnSaveExemption = new JButton("Gem");
	
	private final JLabel lblNotificationSaved = new JLabel(new ImageIcon(MainW.class.getResource("/resources/Complete.png")), JLabel.CENTER);
	private final JLabel lblExemptionSaved = new JLabel(new ImageIcon(MainW.class.getResource("/resources/Complete.png")), JLabel.CENTER);
	
	private final JList lstAgentNotifications = new JList();
	
	public void setTeamLeaderInitials(ArrayList<String> teamleaders){
		if(teamleaders == null)
			this.teamLeaderInitials = new ArrayList<String>();
		else{
			this.teamLeaderInitials = teamleaders;
		}
	}
	
	public void setAgent(AbsenceAnalysisAgent agent){
		this.lblExemptionSaved.setVisible(false);
		this.lblNotificationSaved.setVisible(false);

		this.selectedAgent = agent;
		
		if (selectedAgent != null){
			this.lblSelectedAgentInitials.setText(selectedAgent.getInitials());
			this.lblSelectedAgentName.setText(selectedAgent.getFullName());
			this.lblSelectedAgentTeam.setText(selectedAgent.getTeamCode());
			this.lblSelectedAgentLeader.setText(selectedAgent.getLeaderInitials());

			this.lstAgentNotifications.setListData(selectedAgent.getNotifications().toArray());
			this.lstAgentExemptions.setListData(selectedAgent.getExemptions().toArray());
			
			this.btnNewNotification.setEnabled(true);
			this.btnNewExemption.setEnabled(true);
		} else {
			this.lblSelectedAgentInitials.setText("-");
			this.lblSelectedAgentName.setText("-");
			this.lblSelectedAgentTeam.setText("-");
			this.lblSelectedAgentLeader.setText("-");

			this.btnNewNotification.setEnabled(true);
			this.lstAgentNotifications.setListData(new SicknessNotification[0]);
			
			this.btnNewExemption.setEnabled(false);
			this.lstAgentExemptions.setListData(new ExemptionFromAnalysis[0]);
		}
		
		this.datePickerNotificationCreated.setEnabled(false);
		this.datePickerNotificationCreated.setDate(null);
		this.datePickerNotificationConfirmed.setEnabled(false);
		this.datePickerNotificationConfirmed.setDate(null);
		this.txtSelectedNotificationLeader.setText("-");
		this.cboNotificationResponsible.setEnabled(false);
		this.cboNotificationResponsible.setSelectedItem(-1);
		
		this.datePickerExemptionStart.setEnabled(false);
		this.datePickerExemptionStart.setDate(null);
		this.datePickerExemptionEnd.setEnabled(false);
		this.datePickerExemptionEnd.setDate(null);
		this.txtExemptionNote.setEnabled(false);
		this.txtExemptionNote.setText(null);
		this.btnDeleteExemption.setEnabled(false);
		this.btnNewExemption.setEnabled(true);
		this.btnSaveExemption.setEnabled(false);
	}
	
	public AgentHistoryPanel() {
		this.setMinimumSize(new Dimension(426, 412));
		this.setPreferredSize(new Dimension(426, 412));
		this.setBorder(new TitledBorder(null, "Agent - detaljer", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.setOpaque(false);
		
		JLabel lblInitials = new JLabel("Initialer:");
		lblInitials.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblInitials.setBounds(28, 31, 60, 14);
		this.add(lblInitials);
		
		this.lblSelectedAgentInitials.setBounds(98, 31, 54, 14);
		this.add(this.lblSelectedAgentInitials);
		
		JLabel lblName = new JLabel("Navn:");
		lblName.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblName.setBounds(165, 31, 46, 14);
		this.add(lblName);
		
		this.lblSelectedAgentName.setBounds(221, 31, 195, 14);
		this.add(this.lblSelectedAgentName);

		JLabel lblTeam = new JLabel("Team:");
		lblTeam.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTeam.setBounds(28, 56, 46, 14);
		this.add(lblTeam);
		
		this.lblSelectedAgentTeam.setBounds(98, 56, 46, 14);
		this.add(this.lblSelectedAgentTeam);
		
		JLabel lblLeader = new JLabel("Leder:");
		lblLeader.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblLeader.setBounds(165, 56, 46, 14);
		this.add(lblLeader);
		
		this.lblSelectedAgentLeader.setBounds(221, 56, 195, 14);
		this.add(this.lblSelectedAgentLeader);
		
		this.add(this.getHistoryTabbedPane());
	}
	
	private JTabbedPane getHistoryTabbedPane(){
		JTabbedPane tabbedPaneAgentDetails = new JTabbedPane(JTabbedPane.TOP);
		tabbedPaneAgentDetails.setBounds(10, 104, 406, 302);
	
		tabbedPaneAgentDetails.addTab("Notifikationer", null, this.getAgentNotificationsPane(), null);
		tabbedPaneAgentDetails.addTab("Undtagelser", null, this.getAgentExemptionsPane(), null);
		
		return tabbedPaneAgentDetails;
	}
	private JDesktopPane getAgentNotificationsPane(){
		JDesktopPane desktopPaneAgentNotifications = new JDesktopPane();
		desktopPaneAgentNotifications.setOpaque(false);
		
		this.lstAgentNotifications.setBounds(10, 11, 170, 222);
		this.lstAgentNotifications.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting() == false)
					AgentHistoryPanel.this.currentNotificationChanged();		
			}
		});
		
		JScrollPane scrollPaneAgentNotifications = new JScrollPane(this.lstAgentNotifications);
		scrollPaneAgentNotifications.setBounds(10, 11, 170, 218);
		desktopPaneAgentNotifications.add(scrollPaneAgentNotifications);
		
		this.btnNewNotification.setEnabled(false);
		this.btnNewNotification.setBounds(109, 244, 89, 23);
		this.btnNewNotification.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AgentHistoryPanel.this.createNotification();
			}
		});
		desktopPaneAgentNotifications.add(this.btnNewNotification);
		
		this.btnDeleteNotification.setEnabled(false);
		this.btnDeleteNotification.setBounds(10, 244, 89, 23);
		this.btnDeleteNotification.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AgentHistoryPanel.this.deleteCurrentNotification();
			}
		});
		desktopPaneAgentNotifications.add(this.btnDeleteNotification);
		
		this.lblNotificationSaved.setVisible(false);
		this.lblNotificationSaved.setBounds(265, 244, 23, 23);
		desktopPaneAgentNotifications.add(this.lblNotificationSaved);
		
		this.btnSaveNotification.setEnabled(false);
		this.btnSaveNotification.setBounds(291, 244, 89, 23);
		this.btnSaveNotification.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AgentHistoryPanel.this.saveCurrentNotification();
			}
		});
		desktopPaneAgentNotifications.add(this.btnSaveNotification);
		
		JLabel lblNotificationCreated = new JLabel("Oprettet:");
		lblNotificationCreated.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNotificationCreated.setBounds(190, 26, 80, 14);
		desktopPaneAgentNotifications.add(lblNotificationCreated);
		
		this.datePickerNotificationCreated.setBorder(null);
		this.datePickerNotificationCreated.setEnabled(false);
		this.datePickerNotificationCreated.getEditor().setEditable(false);
		this.datePickerNotificationCreated.setBounds(280, 22, 100, 22);
		this.datePickerNotificationCreated.setFormats(new SimpleDateFormat("dd-MM-yyyy"));
		this.datePickerNotificationCreated.getMonthView().setUpperBound(new Date());
		this.datePickerNotificationCreated.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Date selectedDate = AgentHistoryPanel.this.datePickerNotificationCreated.getDate();
				
				AgentHistoryPanel.this.datePickerNotificationConfirmed.getMonthView().setLowerBound(selectedDate);
				AgentHistoryPanel.this.datePickerNotificationExpired.getMonthView().setLowerBound(selectedDate);
			}
		});
		desktopPaneAgentNotifications.add(this.datePickerNotificationCreated);
		
		this.datePickerNotificationConfirmed.setBorder(null);
		this.datePickerNotificationConfirmed.setEnabled(false);
		this.datePickerNotificationConfirmed.getEditor().setEditable(false);
		this.datePickerNotificationConfirmed.setBounds(280, 55, 100, 22);
		this.datePickerNotificationConfirmed.setFormats(new SimpleDateFormat("dd-MM-yyyy"));
//		this.datePickerNotificationConfirmed.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				if (JXDatePicker.COMMIT_KEY.equals(e.getActionCommand())){
//					DateTime notificationCreated = ((SicknessNotification)AgentHistoryPanel.this.lstAgentNotifications.getSelectedValue()).getCreated();
//					
//					AgentHistoryPanel.this.datePickerNotificationExpired.setDate(null);
//					AgentHistoryPanel.this.datePickerNotificationExpired.getMonthView().setLowerBound(notificationCreated.toDate());
//				}
//			}
//		});
		desktopPaneAgentNotifications.add(this.datePickerNotificationConfirmed);

		JLabel lblNotificationConfirmed = new JLabel("M\u00F8de afholdt:");
		lblNotificationConfirmed.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNotificationConfirmed.setBounds(190, 59, 80, 14);
		desktopPaneAgentNotifications.add(lblNotificationConfirmed);
				
		JLabel lblNotificationLeader = new JLabel("Leder:");
		lblNotificationLeader.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNotificationLeader.setBounds(190, 92, 80, 14);
		desktopPaneAgentNotifications.add(lblNotificationLeader);
		
		this.txtSelectedNotificationLeader.setBounds(280, 92, 100, 14);
		desktopPaneAgentNotifications.add(txtSelectedNotificationLeader);
		
		JLabel lblNotificationResponsible = new JLabel("Ansvarlig");
		lblNotificationResponsible.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNotificationResponsible.setBounds(190, 125, 80, 14);
		desktopPaneAgentNotifications.add(lblNotificationResponsible);
		
		this.cboNotificationResponsible.setEnabled(false);
		
		this.cboNotificationResponsible.setBounds(280, 121, 100, 22);
		desktopPaneAgentNotifications.add(this.cboNotificationResponsible);
		
		JLabel lblNotificationExpired = new JLabel("Ikke afholdt:");
		lblNotificationExpired.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNotificationExpired.setBounds(190, 201, 80, 14);
		desktopPaneAgentNotifications.add(lblNotificationExpired);
	
		this.datePickerNotificationExpired.setEnabled(false);
		this.datePickerNotificationExpired.setBorder(null);
		this.datePickerNotificationExpired.getEditor().setEditable(false);
		this.datePickerNotificationExpired.setBounds(280, 197, 100, 22);
		this.datePickerNotificationExpired.setFormats(new SimpleDateFormat("dd-MM-yyyy"));
//		this.datePickerNotificationExpired.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				if (JXDatePicker.COMMIT_KEY.equals(e.getActionCommand())){
//					DateTime notificationCreated = ((SicknessNotification)AgentHistoryPanel.this.lstAgentNotifications.getSelectedValue()).getCreated();
//					
//					AgentHistoryPanel.this.datePickerNotificationConfirmed.setDate(null);
//					AgentHistoryPanel.this.datePickerNotificationConfirmed.getMonthView().setLowerBound(notificationCreated.toDate());
//				}
//			}
//		});
		desktopPaneAgentNotifications.add(datePickerNotificationExpired);
	
		return desktopPaneAgentNotifications;
	}
	private JDesktopPane getAgentExemptionsPane(){
		JDesktopPane desktopPaneAgentExemptions = new JDesktopPane();
		
		desktopPaneAgentExemptions.setOpaque(false);
		
		this.lstAgentExemptions.setBounds(10, 11, 170, 222);
		this.lstAgentExemptions.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(e.getValueIsAdjusting() == false)
					AgentHistoryPanel.this.currentExemptionSelectionChanged();
			}
		});
		
		JScrollPane exemptionListScrollPane = new JScrollPane(this.lstAgentExemptions);
		exemptionListScrollPane.setBounds(10, 11, 170, 222);
		desktopPaneAgentExemptions.add(exemptionListScrollPane);
		
		JLabel lblExemptionStart = new JLabel("Start:");
		lblExemptionStart.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblExemptionStart.setBounds(190, 26, 46, 14);
		desktopPaneAgentExemptions.add(lblExemptionStart);
		
		JLabel lblExemptionEnd = new JLabel("Slut:");
		lblExemptionEnd.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblExemptionEnd.setBounds(190, 59, 77, 14);
		desktopPaneAgentExemptions.add(lblExemptionEnd);
		
		this.datePickerExemptionStart.getEditor().setEditable(false);
		this.datePickerExemptionStart.setBorder(null);
		this.datePickerExemptionStart.setEnabled(false);
		this.datePickerExemptionStart.setFormats(new SimpleDateFormat("dd-MM-yyyy"));
		this.datePickerExemptionStart.setBounds(280, 22, 100, 22);
		desktopPaneAgentExemptions.add(this.datePickerExemptionStart);
		
		this.datePickerExemptionEnd.getEditor().setEditable(false);
		this.datePickerExemptionEnd.setBorder(null);
		this.datePickerExemptionEnd.setEnabled(false);
		this.datePickerExemptionEnd.setFormats(new SimpleDateFormat("dd-MM-yyyy"));
		this.datePickerExemptionEnd.setBounds(280, 55, 100, 22);
		desktopPaneAgentExemptions.add(this.datePickerExemptionEnd);
		
		JLabel lblExemptionNote = new JLabel("Note:");
		lblExemptionNote.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblExemptionNote.setBounds(190, 90, 46, 14);
		desktopPaneAgentExemptions.add(lblExemptionNote);

		this.txtExemptionNote.setWrapStyleWord(true);
		this.txtExemptionNote.setLineWrap(true);
		this.txtExemptionNote.setBorder(new MatteBorder(1, 1, 1, 1, (Color) SystemColor.menu));
		this.txtExemptionNote.setColumns(10);
		this.txtExemptionNote.setEnabled(false);
		this.txtExemptionNote.setBounds(240, 79, 140, 154);
		this.txtExemptionNote.setFont(new Font("Verdana", Font.PLAIN, 10));
		
		JScrollPane scrollPaneExemptionNote = new JScrollPane(this.txtExemptionNote);
		scrollPaneExemptionNote.setBounds(240, 88, 140, 145);
		desktopPaneAgentExemptions.add(scrollPaneExemptionNote);
		
		this.btnDeleteExemption.setEnabled(false);
		this.btnDeleteExemption.setBounds(10, 244, 89, 23);
		this.btnDeleteExemption.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AgentHistoryPanel.this.deleteCurrentExemption();
			}
		});
		desktopPaneAgentExemptions.add(this.btnDeleteExemption);
		
		this.btnNewExemption.setEnabled(false);
		this.btnNewExemption.setBounds(109, 244, 89, 23);
		this.btnNewExemption.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AgentHistoryPanel.this.createExemption();
			}
		});
		desktopPaneAgentExemptions.add(this.btnNewExemption);
		
		this.btnSaveExemption.setEnabled(false);
		this.btnSaveExemption.setBounds(291, 244, 89, 23);
		this.btnSaveExemption.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AgentHistoryPanel.this.saveCurrentExemption();
			}
		});
		desktopPaneAgentExemptions.add(this.btnSaveExemption);
				
		this.lblExemptionSaved.setVisible(false);
		this.lblExemptionSaved.setBounds(265, 244, 23, 23);
		desktopPaneAgentExemptions.add(this.lblExemptionSaved);
		
		return desktopPaneAgentExemptions;
	}
	
	protected void currentNotificationChanged(){
		SicknessNotification currentNotification = (SicknessNotification)AgentHistoryPanel.this.lstAgentNotifications.getSelectedValue();
		
		this.datePickerNotificationConfirmed.setEnabled(true);
		this.cboNotificationResponsible.setEnabled(true);
		this.cboNotificationResponsible.setSelectedIndex(-1);
		this.cboNotificationResponsible.setModel(new DefaultComboBoxModel(this.teamLeaderInitials.toArray()));
		this.datePickerNotificationExpired.setEnabled(true);
		
		if(currentNotification != null){
			this.datePickerNotificationCreated.setEnabled(false);
			this.datePickerNotificationCreated.setDate(currentNotification.getCreated().toDate());
			
			this.datePickerNotificationConfirmed.getMonthView().setLowerBound(currentNotification.getCreated().toDate());
			
			if(currentNotification.getConfirmed() != null)
				this.datePickerNotificationConfirmed.setDate(currentNotification.getConfirmed().toDate());
			else
				this.datePickerNotificationConfirmed.setDate(null);
			
			this.txtSelectedNotificationLeader.setText(currentNotification.getLeaderInitials());
			
			if(!this.teamLeaderInitials.contains(currentNotification.getResponsibleForMeetingInitials()))
				this.cboNotificationResponsible.addItem(currentNotification.getResponsibleForMeetingInitials());
				
			this.cboNotificationResponsible.setSelectedItem(currentNotification.getResponsibleForMeetingInitials());
			
			this.btnDeleteNotification.setEnabled(true);
			this.btnNewNotification.setEnabled(true);

			this.datePickerNotificationExpired.getMonthView().setLowerBound(currentNotification.getCreated().toDate());
			
			if(currentNotification.getExpired() != null)
				this.datePickerNotificationExpired.setDate(currentNotification.getExpired().toDate());
			else
				this.datePickerNotificationExpired.setDate(null);
		} else {
			this.datePickerNotificationCreated.setEnabled(true);
			this.datePickerNotificationCreated.setDate(new Date());
			
			this.datePickerNotificationConfirmed.setDate(null);
			this.datePickerNotificationConfirmed.getMonthView().setLowerBound(new Date());
			
			this.txtSelectedNotificationLeader.setText(this.lblSelectedAgentLeader.getText());
			
			this.cboNotificationResponsible.setSelectedItem(this.lblSelectedAgentLeader.getText());
			
			this.btnDeleteNotification.setEnabled(false);
			this.btnNewNotification.setEnabled(false);
			
			this.datePickerNotificationExpired.setEnabled(true);
			this.datePickerNotificationExpired.setDate(null);
			this.datePickerNotificationExpired.getMonthView().setLowerBound(new Date());
		}
		
		this.btnSaveNotification.setEnabled(true);
	}
	
	private void createNotification(){
		this.lstAgentNotifications.clearSelection();
		
		this.lblNotificationSaved.setVisible(false);
		
		this.currentNotificationChanged();
	}
	private void saveCurrentNotification(){
		if(!this.validateNotificationDetails()){
			JOptionPane.showMessageDialog(AgentHistoryPanel.this, 
					  					  "Se r�de markeringer.",
					  					  "Fejl i indtastning.", 
					  					  JOptionPane.INFORMATION_MESSAGE);
			
			return;
		}
		
		SicknessNotification notification;
		
		try{
			if(this.lstAgentNotifications.getSelectedValue() == null)
				notification = new SicknessNotification(this.selectedAgent, 
										 				   new DateTime(this.datePickerNotificationCreated.getDate()), 
										 				   (this.datePickerNotificationConfirmed.getDate() != null ? new DateTime(this.datePickerNotificationConfirmed.getDate()) : null),
										 				   (this.datePickerNotificationExpired.getDate() != null ? new DateTime(this.datePickerNotificationExpired.getDate()) : null),
										 				   this.lblSelectedAgentLeader.getText(),
										 				   (String)this.cboNotificationResponsible.getSelectedItem());
			else{
				notification = (SicknessNotification)this.lstAgentNotifications.getSelectedValue();
				
				notification.setConfirmed((this.datePickerNotificationConfirmed.getDate() != null ? new DateTime(this.datePickerNotificationConfirmed.getDate()) : null));
				notification.setResponsibleForMeeting((String)this.cboNotificationResponsible.getSelectedItem());
				notification.setExpired((this.datePickerNotificationExpired.getDate() != null ? new DateTime(this.datePickerNotificationExpired.getDate()) : null));
			}
			
			notification.save();
			
			this.lstAgentNotifications.setListData(this.selectedAgent.getNotifications().toArray());
			this.lblNotificationSaved.setVisible(true);
			
			this.lstAgentNotifications.setSelectedValue(notification, true);
			
			this.raiseNotificationSavedEvent();
		}catch(DopeDBException ex){
			JOptionPane.showMessageDialog(this, 
  					  					  "Der skete en fejl ved oprettelse af ny notifikation. Det anbefales at du lukker programmet ned og starter forfra. \n \nDetaljer: " + ex.getDopeErrorMsg(), 
  					  					  "Fejl", 
  					  					  JOptionPane.ERROR_MESSAGE);
		}
	}
	private void deleteCurrentNotification(){
		if(this.lstAgentNotifications.getSelectedValue() instanceof SicknessNotification){
			SicknessNotification selectedNotification = (SicknessNotification)this.lstAgentNotifications.getSelectedValue();
			
			try{
				selectedNotification.delete();
				
				this.lstAgentNotifications.setListData(this.selectedAgent.getNotifications().toArray());
				this.lstAgentNotifications.validate();
				
				this.raiseNotificationDeletedEvent();
			}catch(DopeDBException ex){
				JOptionPane.showMessageDialog(this, 
	  					  					  "Der skete en fejl ved sletning af notifikation. Det anbefales at du lukker programmet ned og starter forfra. \n \nDetaljer: " + ex.getDopeErrorMsg(), 
	  					  					  "Fejl", 
	  					  					  JOptionPane.ERROR_MESSAGE);
			}
		}
		
		this.lblNotificationSaved.setVisible(false);
	}
	
	private boolean validateNotificationDetails(){
		boolean validated = true;
		
		this.datePickerNotificationCreated.setBorder(new EmptyBorder(0, 0, 0, 0));
		this.datePickerNotificationConfirmed.setBorder(new EmptyBorder(0, 0, 0, 0));
		
		if(this.datePickerNotificationCreated.getDate() == null){
			this.datePickerNotificationCreated.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.RED));
			
			validated = false;
		}else
			if(this.datePickerNotificationConfirmed.getDate() != null)
				if(this.datePickerNotificationConfirmed.getDate().before(this.datePickerNotificationCreated.getDate())){
					this.datePickerNotificationConfirmed.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.RED));
					
					validated = true;					
				}
		
		return validated;
	}
	
	protected void currentExemptionSelectionChanged(){
		ExemptionFromAnalysis currentExemption = (ExemptionFromAnalysis)AgentHistoryPanel.this.lstAgentExemptions.getSelectedValue();
		
		this.datePickerExemptionStart.setEnabled(true);
		this.datePickerExemptionEnd.setEnabled(true);
		this.txtExemptionNote.setEnabled(true);
		this.lblNotificationSaved.setVisible(false);
		
		if (currentExemption != null){
			this.datePickerExemptionStart.setDate(currentExemption.getStartOfExemption().toDate());
			this.datePickerExemptionEnd.setDate(currentExemption.getEndOfExemption() != null ? currentExemption.getEndOfExemption().toDate() : null);
			this.txtExemptionNote.setText(currentExemption.getNote());
			
			this.btnDeleteExemption.setEnabled(true);
			this.btnNewExemption.setEnabled(true);
		} else {
			this.datePickerExemptionStart.setDate(new Date());
			this.datePickerExemptionEnd.setDate(null);
			
			this.txtExemptionNote.setText(null);
			
			this.btnDeleteExemption.setEnabled(false);
			this.btnNewExemption.setEnabled(false);
		}
		
		this.btnSaveExemption.setEnabled(true);
	}
	
	private void createExemption(){
		this.lstAgentExemptions.clearSelection();
		
		this.currentExemptionSelectionChanged();
	}
	private void saveCurrentExemption(){
		if(!this.validateExemptionDetails()){
			JOptionPane.showMessageDialog(this, 
					  					  "Se r�de markeringer.",
					  					  "Fejl i indtastning.", 
					  					  JOptionPane.INFORMATION_MESSAGE);
			
			return;
		}
		
		try {
			if (this.lstAgentExemptions.getSelectedValue() == null)
				new ExemptionFromAnalysis(this.selectedAgent, 
									      LocalDate.fromDateFields(this.datePickerExemptionStart.getDate()), 
										  (this.datePickerExemptionEnd.getDate() != null ? LocalDate.fromDateFields(this.datePickerExemptionEnd.getDate()) : null), 
										  this.txtExemptionNote.getText()).save();
			else if (this.lstAgentExemptions.getSelectedValue() instanceof ExemptionFromAnalysis) {
				ExemptionFromAnalysis selectedExemption = (ExemptionFromAnalysis)this.lstAgentExemptions.getSelectedValue();
				
				selectedExemption.setStartOfExemption(LocalDate.fromDateFields(this.datePickerExemptionStart.getDate()));
				selectedExemption.setEndOfExemption(LocalDate.fromDateFields(this.datePickerExemptionEnd.getDate()));
				selectedExemption.setNote(this.txtExemptionNote.getText());
				
				selectedExemption.save();
			}
			
			this.lblExemptionSaved.setVisible(true);
			
			this.lstAgentExemptions.setListData(this.selectedAgent.getExemptions().toArray());			
			this.lstAgentExemptions.validate();

			this.raiseExemptionSavedEvent();
		} catch (DopeDBException e) {
			JOptionPane.showMessageDialog(this, 
					  					  "Der skete en fejl ved oprettelse af ny undtagelse. Det anbefales at du lukker programmet ned og starter forfra. \n \n Detaljer: " + e.getDopeErrorMsg(), 
					  					  "Fejl", 
					  					  JOptionPane.ERROR_MESSAGE);
		} catch (IllegalArgumentException e){
			JOptionPane.showMessageDialog(this, 
										  e.getMessage(),
										  "Fejl i indtastning.", 
										  JOptionPane.INFORMATION_MESSAGE);
		}
	}
	private void deleteCurrentExemption(){
		if(this.lstAgentExemptions.getSelectedValue() instanceof ExemptionFromAnalysis){
			ExemptionFromAnalysis selectedExemption = (ExemptionFromAnalysis)this.lstAgentExemptions.getSelectedValue();
		
			try{
				selectedExemption.delete();				
			} catch(DopeDBException ex) {
				JOptionPane.showMessageDialog(this, 
	  					  					  "Der skete en fejl ved sletning af undtagelse. Det anbefales at du lukker programmet ned og starter forfra. /n /n Detaljer: " + ex.getDopeErrorMsg(), 
	  					  					  "Fejl", 
	  					  					  JOptionPane.ERROR_MESSAGE);
			}
			
			this.lstAgentExemptions.setListData(this.selectedAgent.getExemptions().toArray());
			this.lstAgentExemptions.validate();
			
			this.raiseExemptionDeletedEvent();
		}		
	}
	
	private boolean validateExemptionDetails(){
		boolean validated = true;
		
		if(this.datePickerExemptionStart.getDate() == null){
			this.datePickerExemptionStart.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.RED));
			
			validated = false;
		}else{
			this.datePickerExemptionStart.setBorder(new EmptyBorder(0, 0, 0, 0));
			
			if(this.datePickerExemptionEnd.getDate() != null && 
			   this.datePickerExemptionEnd.getDate().before(this.datePickerExemptionStart.getDate())){
				this.datePickerExemptionEnd.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.RED));
				
				validated = false;
			}else{
				this.datePickerExemptionEnd.setBorder(new EmptyBorder(0, 0, 0, 0));
			}
			
		}
		
		if(this.txtExemptionNote.getText().length() > 1000){
			this.txtExemptionNote.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.RED));
			
			validated = false;
		} else
			this.txtExemptionNote.setBorder(new EmptyBorder(1, 1, 1, 1));	
		
		return validated;
	}
	
	public void addListener(AgentHistoryPanelListener listener){
		this.listeners.add(listener);
	}
	public void removeListeners(AgentHistoryPanelListener listener){
		this.listeners.remove(listener);
	}
	
	private void raiseNotificationSavedEvent(){
		for(AgentHistoryPanelListener l : this.listeners)
			l.notificationSaved();
	}
	private void raiseNotificationDeletedEvent(){
		for(AgentHistoryPanelListener l : this.listeners)
			l.notificationDeleted();
	}
	
	private void raiseExemptionSavedEvent(){
		for(AgentHistoryPanelListener l : this.listeners)
			l.exemptionSaved();
	}
	private void raiseExemptionDeletedEvent(){
		for(AgentHistoryPanelListener l : this.listeners)
			l.exemptionDeleted();
	}
}