package absenceAnalysis;

import java.util.ArrayList;
import org.joda.time.LocalDate;

public class SicknessPeriod {

	private ArrayList<LocalDate> sickDays = new ArrayList<LocalDate>();
	
	public ArrayList<LocalDate> getSickDays(){
		return this.sickDays;
	}
	
	public LocalDate getStart(){
		if(this.sickDays.size() == 0)
			return null;
		else{
			LocalDate startOfSicknessPeriod = this.sickDays.get(0);
		
			for(LocalDate d : this.sickDays)
				if(d.isBefore(startOfSicknessPeriod))
					startOfSicknessPeriod = d;
				
			return startOfSicknessPeriod;
		}
	}
	public LocalDate getEnd(){
		if(this.sickDays.size() == 0)
			return null;
		else{
			LocalDate endOfSicknessPeriod = this.sickDays.get(0);
		
			for(LocalDate d : this.sickDays)
				if(d.isAfter(endOfSicknessPeriod))
					endOfSicknessPeriod = d;
				
			return endOfSicknessPeriod;
		}
	}
	
	public SicknessPeriod(){
	}
	public SicknessPeriod(ArrayList<LocalDate> sickDays){
		this.sickDays = sickDays;
	}
	@Override
	public String toString() {
		if(this.sickDays.size() == 0)
			return "-";
		else if(this.sickDays.size() == 1)
			return this.getStart().toString("dd-MM-yyyy");
		else
			return (this.getStart().toString("dd-MM-yyyy") + " - " + this.getEnd().toString("dd-MM-yyyy"));
	}
	
	
}
