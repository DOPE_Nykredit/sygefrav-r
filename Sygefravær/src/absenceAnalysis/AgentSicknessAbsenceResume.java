package absenceAnalysis;

import java.util.ArrayList;

import org.joda.time.LocalDate;

public class AgentSicknessAbsenceResume {
	
	private SicknessPeriod[] sicknessperiods;
	private LocalDate[] daysOff; 
	
	private AbsenceAnalysisAgent agent;
	
	private int scheduledWorkdaysCount;
	
	private double scheduledWorkHoursCount;
	private double hoursWithSicknessCount;
	
	public SicknessPeriod[] getSicknessPeriods(){
		return this.sicknessperiods;
	}
	public int getSicknessPeriodCount(){
		return this.sicknessperiods.length;
	}
	public int getSicknessPeriodCount(LocalDate fromDate, LocalDate toDate){
		int count = 0;
		
		for(SicknessPeriod s : this.sicknessperiods)
			if(s.getStart().isBefore(toDate) || s.getEnd().isBefore(fromDate))
				count += 1;
		
		return count;
	}
	public SicknessPeriod getLatestSicknessPeriod(){
		SicknessPeriod latestPeriod = null;
		
		if(this.sicknessperiods.length > 0){
			latestPeriod = this.sicknessperiods[0];
			
			if(this.sicknessperiods.length > 1)
				for(SicknessPeriod s : this.sicknessperiods)
					if(s.getStart().isAfter(latestPeriod.getEnd()))
						latestPeriod = s;
		}
		
		return latestPeriod;
	}

	public LocalDate[] getDaysOff(){
		return this.daysOff;
	}
	public LocalDate[] getSickDays(){
		ArrayList<LocalDate> sickDays = new ArrayList<LocalDate>();
		
		for(SicknessPeriod s : this.sicknessperiods)
			for(LocalDate d : s.getSickDays())
				sickDays.add(d);
		
		return sickDays.toArray(new LocalDate[0]);
	}
	
	public AbsenceAnalysisAgent getAgent(){
		return this.agent;
	}
	
	public int getScheduledWorkdaysCount(){
		return this.scheduledWorkdaysCount;
	}
	public int getSickDaysCount(){
		int sickDaysCount = 0;
		
		for(SicknessPeriod s : this.sicknessperiods)
			sickDaysCount += s.getSickDays().size();
		
		return sickDaysCount;
	}
	
	public double getScheduledWorkHoursCount(){
		return this.scheduledWorkHoursCount;
	}
	public double getHoursWithSicknessCount(){
		return this.hoursWithSicknessCount;
	}
	
	public boolean isDueForNotification(int monthsToCalculate, int sicknessPeriodsLimit, int maxDaysInSicknessPeriod){
		if(this.getSicknessPeriodCount() > 0){
			LocalDate calculateFrom = this.getLatestSicknessPeriod().getStart().minusMonths(monthsToCalculate);
			ArrayList<SicknessPeriod> sicknessPeriodsToConsider = new ArrayList<SicknessPeriod>();

			for(SicknessPeriod s : this.sicknessperiods)
				if(!this.agent.isSicknessPeriodCoveredByExemption(s) && s.getEnd().isAfter(calculateFrom))
					sicknessPeriodsToConsider.add(s);
			
			SicknessNotification latestNotification = this.agent.getLatestNotification();
			
			if(sicknessPeriodsToConsider.size() >= sicknessPeriodsLimit){				
				if(latestNotification != null){
					for(SicknessPeriod s : sicknessPeriodsToConsider)
						if(s.getStart().isAfter(latestNotification.getCreated().toLocalDate()))
							return true;
				}else if(latestNotification == null)
					return true;						
			}else{
				for(SicknessPeriod s : sicknessPeriodsToConsider){
					if(s.getSickDays().size() >= maxDaysInSicknessPeriod)
						if((latestNotification != null && s.getEnd().isAfter(latestNotification.getCreated().toLocalDate())) || latestNotification == null)
							return true;
				}
			}
		}
			
		return false;
	}
	
	public ArrayList<SicknessPeriod> getDueSicknessPeriods(){
		ArrayList<SicknessPeriod> dueSicknessPeriods = new ArrayList<SicknessPeriod>();
		
		for(SicknessPeriod s : this.sicknessperiods){
			if(s.getEnd().isAfter(LocalDate.now().minusMonths(3)) && (this.agent.getLatestNotification() != null || s.getEnd().isAfter(this.agent.getLatestNotification().getCreated().toLocalDate()))){
				dueSicknessPeriods.add(s);
			}
		}
		
		return dueSicknessPeriods;
	}
	
	public AgentSicknessAbsenceResume(AbsenceAnalysisAgent agent,
								 	  int scheduledWorkdaysCount,
								 	  double scheduledWorkMinutesCount,
								 	  double hoursWithSicknessCount,
								 	  SicknessPeriod[] sicknessPeriods,
								 	  LocalDate[] daysOff){
		this.agent = agent;
		this.scheduledWorkdaysCount = scheduledWorkdaysCount;
		this.scheduledWorkHoursCount = scheduledWorkMinutesCount / 60;
		this.hoursWithSicknessCount = hoursWithSicknessCount;
		this.sicknessperiods = sicknessPeriods;
		this.daysOff = daysOff;
	}
}