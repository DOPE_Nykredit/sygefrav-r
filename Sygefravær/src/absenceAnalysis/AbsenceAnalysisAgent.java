package absenceAnalysis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import org.joda.time.LocalDate;

import com.nykredit.kundeservice.data.DopeException;
import com.nykredit.kundeservice.data.KSDriftConnection;
import com.nykredit.kundeservice.data.sql.OracleFormats;
import com.nykredit.kundeservice.data.sql.SqlQuery;
import com.nykredit.kundeservice.util.OperationAbortedException;
import com.nykredit.kundeservice.workforce.AgentSchedule;
import com.nykredit.kundeservice.workforce.ExceptionInWorkDay;
import com.nykredit.kundeservice.workforce.WorkDay;
import com.nykredit.kundeservice.workforce.WorkForceData;
import com.nykredit.kundeservice.workforce.ExceptionInWorkDay.WorkdayExceptionType;

public class AbsenceAnalysisAgent extends AgentSchedule {
	
	private String teamCode;
	private String leaderInitials;
	private String teamGroup;
	
	private ArrayList<ExemptionFromAnalysis> exemptions = new ArrayList<ExemptionFromAnalysis>();
	private ArrayList<SicknessNotification> notifications = new ArrayList<SicknessNotification>();

	public String getTeamCode(){
		return this.teamCode;
	}
	public String getTeamGroup(){
		return this.teamGroup;
	}
	public String getLeaderInitials(){
		return this.leaderInitials;
	}
	
	public ArrayList<ExemptionFromAnalysis> getExemptions(){
		return this.exemptions;
	}
	public ArrayList<SicknessNotification> getNotifications(){
		Collections.sort(this.notifications, new SicknessNotificationComparator());
		
		return this.notifications;
	}
	public SicknessNotification getLatestNotification(){
		if(this.notifications.size() == 0)
			return null;
		else {
			SicknessNotification latestNotification = this.notifications.get(0);
			
			for(SicknessNotification n : this.notifications)
				if(latestNotification.getCreated().isBefore(n.getCreated()))
					latestNotification = n;
			
			return latestNotification;
		}			
	}
	
	public boolean hasCurrentExemption(){
		LocalDate now = new LocalDate();
		
		for (ExemptionFromAnalysis e : this.exemptions){
			if (e.getStartOfExemption().isBefore(now) && e.getEndOfExemption() == null)
				return true;
			if (e.getStartOfExemption().isBefore(now) && e.getEndOfExemption().isAfter(now))
				return true;
		}
		
		return false;
	}
	
	public boolean isSicknessPeriodCoveredByExemption(SicknessPeriod period){
		boolean periodIsCovered = false;
		
		for(ExemptionFromAnalysis e : this.exemptions)
			if(e.getStartOfExemption().isBefore(period.getEnd()) || e.getStartOfExemption().equals(period.getEnd())){
				if(e.getEndOfExemption() == null)
					return true;
				else if(e.getEndOfExemption().isAfter(period.getStart()))
					return true;
				else return false;
			}
		
		return periodIsCovered;
	}
	
	public AgentSicknessAbsenceResume getSicknessResume(){		
		Collections.sort(super.getWorkdays(), new workdayComparator());
		
		ArrayList<SicknessPeriod> sicknessPeriods = new ArrayList<SicknessPeriod>();
		ArrayList<LocalDate> daysOff = new ArrayList<LocalDate>();
		
		SicknessPeriod currentSicknessPeriod = null;
		
		int scheduledDaysCount = 0;
		
		double scheduledWorkMinutesCount = 0;
		double hoursWithSicknessCount = 0;
		
		for (WorkDay w : this.getWorkdays()){
			if((w.hasFullDayException() || w.hasExceptionCoveringEntireShift()) &&  
			  (w.getExceptions().get(0).getType() == WorkdayExceptionType.FERIE_OMSORG ||
			  w.getExceptions().get(0).getType() == WorkdayExceptionType.FRI ||
			  w.getExceptions().get(0).getType() == WorkdayExceptionType.HELLIGDAG ||
			  w.getExceptions().get(0).getType() == WorkdayExceptionType.TELEKOMPENSRING)){
				if(w.getExceptions().get(0).getType() != WorkdayExceptionType.HELLIGDAG)
					daysOff.add(w.getDate());
			}else{
				scheduledDaysCount += 1;
				scheduledWorkMinutesCount += w.getWorkMinutes();
				
				if (w.hasExceptionOfType(ExceptionInWorkDay.WorkdayExceptionType.SYG)){
					if (w.hasFullDayException())
						hoursWithSicknessCount += 7.5;
					else
						for (ExceptionInWorkDay e : w.getExceptions())
							if (e.getType() == ExceptionInWorkDay.WorkdayExceptionType.SYG)
								hoursWithSicknessCount += e.getDurationHours();
					
					if(currentSicknessPeriod == null){
						currentSicknessPeriod = new SicknessPeriod();
					}
					
					currentSicknessPeriod.getSickDays().add(w.getDate());						
				}else if(w.getWorkMinutes() > 0){
					if(currentSicknessPeriod != null){
						sicknessPeriods.add(currentSicknessPeriod);
						currentSicknessPeriod = null;
					}				
				}
			}
		}
		
		return new AgentSicknessAbsenceResume(this, 
											  scheduledDaysCount, 
										      scheduledWorkMinutesCount, 
										      hoursWithSicknessCount,
										      sicknessPeriods.toArray(new SicknessPeriod[0]),
										      daysOff.toArray(new LocalDate[0]));
	}

	public AbsenceAnalysisAgent(int agentId, String initials, String firstName, String lastName, String teamCode, String teamGroup, String leaderInitials){
		super(agentId, initials, firstName, lastName);
		this.teamCode = teamCode;
		this.teamGroup = teamGroup;
		this.leaderInitials = leaderInitials;
	}
	public AbsenceAnalysisAgent(AgentSchedule agent, String teamCode, String teamGroup, String leaderInitials){
		super(agent.getAgentId(),
			  agent.getInitials(),
			  agent.getFirstName(),
			  agent.getLastName());
		super.getWorkdays().addAll(agent.getWorkdays());
		
		this.teamCode = teamCode;
		this.teamGroup = teamGroup;
		this.leaderInitials = leaderInitials;
	}
	
	public double getSicknessPercentage(int monthOfYear, int year){
		double sicknessMinutes = 0;
		double totalMinutes = 0;
		
		for(WorkDay w : this.getWorkdays()){
			if(w.getDate().getMonthOfYear() == monthOfYear && w.getDate().getYear() == year){
				for(ExceptionInWorkDay e : w.getExceptions()){
					if(e.getType() == WorkdayExceptionType.SYG)
						if(e.getSpan() == null){
							sicknessMinutes += 60 * 7.5;
							totalMinutes += 60 * 7.5;
						}else
							sicknessMinutes += e.getDurationInMinutes();
				}
				
				if(w.getSpan() != null)
					totalMinutes += w.getSpan().toDuration().getStandardMinutes();
			}
		}
		
		if(totalMinutes > 0)
			return sicknessMinutes / totalMinutes * 100;
		else
			return 0;
	}
	public double getSicknessPercentage(LocalDate fromDate, LocalDate toDate){
		int sicknessMinutes = 0;
		int totalMinutes = 0;
		
		for(WorkDay w : this.getWorkdays()){
			if((w.getDate().isAfter(fromDate) || w.getDate().equals(fromDate)) && (w.getDate().isBefore(toDate) || w.getDate().equals(toDate))){
				for(ExceptionInWorkDay e : w.getExceptions()){
					if(e.getType() == WorkdayExceptionType.SYG)
						if(e.getSpan() == null){
							sicknessMinutes += 60 * 7.5;
							totalMinutes += 60 * 7.5;
						}else
							sicknessMinutes += e.getDurationInMinutes();
				}
				
				if(w.getSpan() != null)
					totalMinutes += w.getSpan().toDuration().getStandardMinutes();
			}
		}
		
		if(totalMinutes > 0)
			return (double)sicknessMinutes / totalMinutes * 100;
		else
			return 0;
	}
	
	public static ArrayList<AbsenceAnalysisAgent> getAllAgents(boolean includeWorkForceData) throws OperationAbortedException{
		ArrayList<AbsenceAnalysisAgent> agents = new ArrayList<AbsenceAnalysisAgent>();
		
		KSDriftConnection conn;
		
		try{
			conn = new KSDriftConnection();
			SqlQuery query = null;
			
			if(includeWorkForceData){
				ArrayList<AgentSchedule> agentSchedules = WorkForceData.getWorkForceDataByAgent(14, conn);

				query = new SqlQuery(conn, "SELECT " + 
										       "vt.INITIALER, " +
										       "tea.TEAM, " +
										       "tea.TEAM_GRUPPE, " +
										       "tea.TEAM_LEDER " +
										   "FROM " +     
										   	   "KS_DRIFT.AGENTER_TEAMS tea, " +
										   	   "KS_DRIFT.V_TEAM_DATO vt " +
										   "WHERE " +
										   	   "tea.ID = vt.TEAM_ID AND " +
											   "vt.DATO = " + OracleFormats.convertDate(new Date())); 
			
				query.execute();
				
				while(query.next()){
					for(AgentSchedule a : agentSchedules)
						if(query.getString("INITIALER").equals(a.getInitials()))
							agents.add(new AbsenceAnalysisAgent(a,
																query.getString("TEAM"),
																query.getString("TEAM_GRUPPE"),
																query.getString("TEAM_LEDER")));
				}
			}else{
				query = new SqlQuery(conn, "SELECT " +  
										       "age.ID, " +
										       "age.INITIALER, " + 
										       "age.FORNAVN, " +
										       "age.EFTERNAVN, " +
										       "team.TEAM, " +
										       "team.TEAM_GRUPPE, " +
										       "team.TEAM_LEDER " +
										   "FROM " +     
										   	   "AGENTER_LISTE age " +
										   "INNER JOIN " + 
										       "V_TEAM_DATO vt ON age.ID  = vt.AGENT_ID " + 
										   "INNER JOIN " + 
										       "AGENTER_TEAMS team ON vt.TEAM_ID = team.ID " +
										   "WHERE " +    
										       "vt.DATO = trunc(sysdate) AND LENGTH(age.INITIALER) < 5 AND team.TEAM <> 'FL'" +
										   "ORDER BY " + 
										       "age.INITIALER");
				
				query.execute();
				
				while(query.next())
					agents.add(new AbsenceAnalysisAgent(query.getInteger("ID"), 
												 	    query.getString("INITIALER"), 
												 	    query.getString("FORNAVN"), 
												 	    query.getString("EFTERNAVN"), 
												 	    query.getString("TEAM"),
												 	    query.getString("TEAM_GRUPPE"),
												 	    query.getString("TEAM_LEDER")));
			}
	
			query = new SqlQuery(conn, "SELECT " +
									       "ID, " +
										   "ex1.AGENT_ID, " +
										   "START_DATE, " +
										   "END_DATE," +
										   "NOTE, " +
										   "CREATED, " +
										   "UPDATED, " +
										   "UPDATEDBY " + 
									   "FROM " +
									       "KS_APPLIKATION.SFS_EXEMPTION ex1 " +
									   "INNER JOIN " +
									   	   "(SELECT " +
									   	        "MAX(END_DATE), " +
												"AGENT_ID " +
											 "FROM " +
											     "KS_APPLIKATION.SFS_EXEMPTION ex2 " +
											 "GROUP BY " +
											     "AGENT_ID) ex2 " +
											 "ON " +
	       									     "ex1.AGENT_ID = ex2.AGENT_ID");

			query.execute();

			while(query.next()){
				for (AbsenceAnalysisAgent a : agents)
					if (a.getAgentId() == query.getInteger("AGENT_ID"))
						a.getExemptions().add(new ExemptionFromAnalysis(query.getInteger("ID"), 
																		a, 
																		query.getLocalDate("START_DATE"), 
																		query.getLocalDate("END_DATE"), 
																		query.getString("NOTE"), 
																		query.getLocalDate("CREATED"), 
																		query.getLocalDate("UPDATED"), 
																		query.getString("UPDATEDBY")));
			}
			
			query = new SqlQuery(conn, "SELECT " +
									       "ID, " +
										   "noti.AGENT_ID, " +
										   "CREATED, " +
										   "CONFIRMED, " +
										   "EXPIRED, " +
										   "CURRENT_LEADER, " +
										   "RESPONSIBLE_LEADER, " +
										   "UPDATED, " +
										   "UPDATEDBY " + 
									   "FROM " +
										   "KS_APPLIKATION.SFS_NOTIFICATION noti " +
									   "INNER JOIN " +
										   "V_TEAM_DATO vt " +
									   "ON " +
									   	   "noti.AGENT_ID = vt.AGENT_ID AND noti.CREATED = vt.DATO");
			
			query.execute();
			
			while(query.next())
				for(AbsenceAnalysisAgent a : agents)
					if(a.getAgentId() == query.getInteger("AGENT_ID"))
						a.getNotifications().add(new SicknessNotification(query.getInteger("ID"),
																		  a, 
																		  query.getDateTime("CREATED"), 
																		  query.getDateTime("CONFIRMED"), 
																		  query.getDateTime("EXPIRED"),
																		  query.getString("CURRENT_LEADER"), 
																		  query.getString("RESPONSIBLE_LEADER"),
																		  query.getDateTime("UPDATED"),
																		  query.getString("UPDATEDBY")));
		
			conn.closeConnection();
		} catch (DopeException e) {
			throw new OperationAbortedException(e, e.getMessage(), e.getDopeErrorMsg());
		}
		
		return agents;
	}
	
	public static ArrayList<String> retrieveLeaderInitials() throws OperationAbortedException{
		ArrayList<String> teamLeaderInitials = new ArrayList<String>();
		
		try {
			SqlQuery query = new SqlQuery(new KSDriftConnection(), "SELECT " + 
															           "* " +  
															       "FROM (" + 
															       	   "SELECT " +
															       	       "TEAM_LEDER " +
															           "FROM " +
															               "KS_DRIFT.AGENTER_TEAMS " +
															           "UNION SELECT " +
															               "TEAM_CHEFER " +
															           "FROM " + 
															               "KS_DRIFT.AGENTER_TEAMS) " +
															       "WHERE " +
															           "TEAM_LEDER is not null");
				
			query.execute();
			
			while(query.next()){
				teamLeaderInitials.add(query.getString("TEAM_LEDER"));
			}
		} catch (DopeException e) {
			throw new OperationAbortedException(e, "Error retrieving teamleader initials from database.", "Der skete en fejl ved indlęsning af lederes initialer.");
		}
		
		return teamLeaderInitials;
	}
	
	private class workdayComparator implements Comparator<WorkDay> {
		@Override
		public int compare(WorkDay o1, WorkDay o2) {
			return o1.getDate().compareTo(o2.getDate());
		}
	}
	
	private class SicknessNotificationComparator implements Comparator<SicknessNotification>{

		@Override
		public int compare(SicknessNotification o1, SicknessNotification o2) {
			return o2.getCreated().compareTo(o1.getCreated());
		}
	}
}
