package absenceAnalysis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.joda.time.LocalDate;

import com.nykredit.kundeservice.data.DopeException;
import com.nykredit.kundeservice.data.KSDriftConnection;
import com.nykredit.kundeservice.data.sql.SqlQuery;
import com.nykredit.kundeservice.util.OperationAbortedException;
import com.nykredit.kundeservice.workforce.AgentSchedule;
import com.nykredit.kundeservice.workforce.ExceptionInWorkDay;
import com.nykredit.kundeservice.workforce.WorkDay;
import com.nykredit.kundeservice.workforce.WorkForceData;
import com.nykredit.kundeservice.workforce.ExceptionInWorkDay.WorkdayExceptionType;

public class AbsenceAnalysisAgent extends AgentSchedule {
	
	private String leaderInitials;

	private ArrayList<ExemptionFromAnalysis> exemptions = new ArrayList<ExemptionFromAnalysis>();
	private ArrayList<SicknessNotification> notifications = new ArrayList<SicknessNotification>();
	
	public String getLeaderInitials(){
		return this.leaderInitials;
	}
	
	public ArrayList<ExemptionFromAnalysis> getExemptions(){
		return this.exemptions;
	}
	public ArrayList<SicknessNotification> getNotifications(){
		return this.notifications;
	}
	public SicknessNotification getLatestNotification(){
		if(this.notifications.size() == 0)
			return null;
		else {
			SicknessNotification latestNotification = this.notifications.get(0);
			
			for(SicknessNotification n : this.notifications)
				if(latestNotification.getCreated().isBefore(n.getCreated()))
					latestNotification = n;
			
			return latestNotification;
		}			
	}
	
	public boolean hasCurrentExemption(){
		LocalDate now = new LocalDate();
		
		for (ExemptionFromAnalysis e : this.exemptions){
			if (e.getStartOfExemption().isBefore(now) && e.getEndOfExemption() == null)
				return true;
			if (e.getStartOfExemption().isBefore(now) && e.getEndOfExemption().isAfter(now))
				return true;
		}
		
		return false;
	}
	
	public boolean isSicknessPeriodCoveredByExemption(SicknessPeriod period){
		boolean periodIsCovered = false;
		
		for(ExemptionFromAnalysis e : this.exemptions)
			if(e.getStartOfExemption().isBefore(period.getEnd()) || e.getStartOfExemption().equals(period.getEnd())){
				if(e.getEndOfExemption() == null)
					return true;
				else if(e.getEndOfExemption().isAfter(period.getStart()))
					return true;
				else return false;
			}
//			if(e.getStartOfExemption().isBefore(period.getEnd()) || (e.getEndOfExemption() != null && e.getEndOfExemption().isAfter(period.getStart())))
//				periodIsCovered = true;
		
		return periodIsCovered;
	}
	
	public AgentSicknessAbsenceResume getSicknessResume(){

		if(this.getInitials() == "GABS")
			System.out.print("BANG");
		
		Collections.sort(super.getWorkdays(), new workdayComparator());
		
		ArrayList<SicknessPeriod> sicknessPeriods = new ArrayList<SicknessPeriod>();
		ArrayList<LocalDate> daysOff = new ArrayList<LocalDate>();
		
		SicknessPeriod currentSicknessPeriod = null;
		
		int scheduledDaysCount = 0;
		
		double scheduledWorkMinutesCount = 0;
		double hoursWithSicknessCount = 0;
		
		for (WorkDay w : this.getWorkdays()){
			if(this.getInitials().equalsIgnoreCase("GABS") && w.getDate().getMonthOfYear() == 5 && w.getDate().getDayOfMonth() == 14)
				System.out.print("BUM");
			
			if((w.hasFullDayException() || w.hasExceptionCoveringEntireShift()) &&  
			  (w.getExceptions().get(0).getType() == WorkdayExceptionType.FERIE_OMSORG ||
			  w.getExceptions().get(0).getType() == WorkdayExceptionType.FRI ||
			  w.getExceptions().get(0).getType() == WorkdayExceptionType.HELLIGDAG ||
			  w.getExceptions().get(0).getType() == WorkdayExceptionType.TELEKOMPENSRING)){
				if(w.getExceptions().get(0).getType() != WorkdayExceptionType.HELLIGDAG)
					daysOff.add(w.getDate());
			}else{
				scheduledDaysCount += 1;
				scheduledWorkMinutesCount += w.getWorkMinutes();
				
				if (w.hasExceptionOfType(ExceptionInWorkDay.WorkdayExceptionType.SYG)){
					if (w.hasFullDayException())
						hoursWithSicknessCount += 7.5;
					else
						for (ExceptionInWorkDay e : w.getExceptions())
							if (e.getType() == ExceptionInWorkDay.WorkdayExceptionType.SYG)
								hoursWithSicknessCount += e.getDurationHours();
					
					if(currentSicknessPeriod == null){
						currentSicknessPeriod = new SicknessPeriod();
					}
					
					currentSicknessPeriod.getSickDays().add(w.getDate());						
				}else if(w.getWorkMinutes() > 0){
					if(currentSicknessPeriod != null){
						sicknessPeriods.add(currentSicknessPeriod);
						currentSicknessPeriod = null;
					}				
				}
			}
		}
		
		return new AgentSicknessAbsenceResume(this, 
											  scheduledDaysCount, 
										      scheduledWorkMinutesCount, 
										      hoursWithSicknessCount,
										      sicknessPeriods.toArray(new SicknessPeriod[0]),
										      daysOff.toArray(new LocalDate[0]));
	}

	public AbsenceAnalysisAgent(int agentId, String initials, String firstName, String lastName, String teamCode, String leaderInitials){
		super(agentId, initials, firstName, lastName, teamCode);
		this.leaderInitials = leaderInitials;
	}
	public AbsenceAnalysisAgent(AgentSchedule agent, String leaderInitials){
		super(agent.getId(),
			  agent.getInitials(),
			  agent.getFirstName(),
			  agent.getLastName(),
			  agent.getTeam());
		super.getWorkdays().addAll(agent.getWorkdays());
		this.leaderInitials = leaderInitials;
	}
	
	public static ArrayList<AbsenceAnalysisAgent> getAllAgents(boolean includeWorkForceData) throws OperationAbortedException{
		ArrayList<AbsenceAnalysisAgent> agents = new ArrayList<AbsenceAnalysisAgent>();
		
		KSDriftConnection conn;
		
		try{
			conn = new KSDriftConnection();
			SqlQuery query = null;
			
			if(includeWorkForceData){
				ArrayList<AgentSchedule> agentSchedules = WorkForceData.getWorkForceDataByAgent(12, conn);

				query = new SqlQuery(conn, "SELECT " +
											   "TEAM, " +
											   "TEAM_LEDER " +
										   "FROM " +
										   	   "KS_DRIFT.AGENTER_TEAMS");
			
				query.execute();
				
				while(query.next()){
					String teamCode = query.getString("TEAM");
					
					for(AgentSchedule a : agentSchedules)
						if(a.getTeam().equalsIgnoreCase(teamCode))
							agents.add(new AbsenceAnalysisAgent(a, query.getString("TEAM_LEDER")));
				}
			}else{
				query = new SqlQuery(conn, "SELECT " +  
										       "age.ID, " +
										       "age.INITIALER, " + 
										       "age.FORNAVN, " +
										       "age.EFTERNAVN, " +
										       "team.TEAM, " +
										       "team.TEAM_LEDER " +
										   "FROM " +     
										   	   "AGENTER_LISTE age " +
										   "INNER JOIN " + 
										       "V_TEAM_DATO vt ON age.ID  = vt.AGENT_ID " + 
										   "INNER JOIN " + 
										       "AGENTER_TEAMS team ON vt.TEAM_ID = team.ID " +
										   "WHERE " +    
										       "vt.DATO = trunc(sysdate) AND LENGTH(age.INITIALER) < 5 " +
										   "ORDER BY " + 
										       "age.INITIALER");
				
				query.execute();
				
				while(query.next())
					agents.add(new AbsenceAnalysisAgent(query.getInteger("ID"), 
												 	    query.getString("INITIALER"), 
												 	    query.getString("FORNAVN"), 
												 	    query.getString("EFTERNAVN"), 
												 	    query.getString("TEAM"),
												 	    query.getString("TEAM_LEDER")));
			}
	
			query = new SqlQuery(conn, "SELECT " +
									       "ID, " +
										   "ex1.AGENT_ID, " +
										   "START_DATE, " +
										   "END_DATE," +
										   "NOTE, " +
										   "CREATED, " +
										   "UPDATED, " +
										   "UPDATEDBY " + 
									   "FROM " +
									       "KS_APPLIKATION.SFS_EXEMPTION ex1 " +
									   "INNER JOIN " +
									   	   "(SELECT " +
									   	        "MAX(END_DATE), " +
												"AGENT_ID " +
											 "FROM " +
											     "KS_APPLIKATION.SFS_EXEMPTION ex2 " +
											 "GROUP BY " +
											     "AGENT_ID) ex2 " +
											 "ON " +
	       									     "ex1.AGENT_ID = ex2.AGENT_ID");

			query.execute();

			while(query.next()){
				for (AbsenceAnalysisAgent a : agents)
					if (a.getId() == query.getInteger("AGENT_ID"))
						a.getExemptions().add(new ExemptionFromAnalysis(query.getInteger("ID"), 
																		a, 
																		query.getLocalDate("START_DATE"), 
																		query.getLocalDate("END_DATE"), 
																		query.getString("NOTE"), 
																		query.getLocalDate("CREATED"), 
																		query.getLocalDate("UPDATED"), 
																		query.getString("UPDATEDBY")));
			}
			
			query = new SqlQuery(conn, "SELECT " +
									       "ID, " +
										   "noti.AGENT_ID, " +
										   "CREATED, " +
										   "CONFIRMED," +
										   "CURRENT_LEADER, " +
										   "RESPONSIBLE_LEADER, " +
										   "UPDATED, " +
										   "UPDATEDBY " + 
									   "FROM " +
										   "KS_APPLIKATION.SFS_NOTIFICATION noti " +
									   "INNER JOIN " +
										   "V_TEAM_DATO vt " +
									   "ON " +
									   	   "noti.AGENT_ID = vt.AGENT_ID AND noti.CREATED = vt.DATO");
			
			query.execute();
			
			while(query.next()){
				for(AbsenceAnalysisAgent a : agents)
					if(a.getId() == query.getInteger("AGENT_ID"))
						a.getNotifications().add(new SicknessNotification(query.getInteger("ID"),
																		  a, 
																		  query.getLocalDate("CREATED"), 
																		  query.getLocalDate("CONFIRMED"), 
																		  query.getString("CURRENT_LEADER"), 
																		  query.getString("RESPONSIBLE_LEADER"),
																		  query.getLocalDate("UPDATED"),
																		  query.getString("UPDATEBY")));
			}
		
			conn.closeConnection();
		} catch (DopeException e) {
			throw new OperationAbortedException(e, e.getMessage(), e.getDopeErrorMsg());
		}
		
		return agents;
	}

	private class workdayComparator implements Comparator<WorkDay> {
		@Override
		public int compare(WorkDay o1, WorkDay o2) {
			return o1.getDate().compareTo(o2.getDate());
		}
	}
}
