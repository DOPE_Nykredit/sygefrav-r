package gui;

import gui.AgentListView.AgentFilter;
import gui.ResumeListView.ResumeFilter;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.nykredit.kundeservice.data.DopeDBException;
import com.nykredit.kundeservice.data.DopeException;
import com.nykredit.kundeservice.data.KSDriftConnection;
import com.nykredit.kundeservice.data.sql.DopeResultSetException;
import com.nykredit.kundeservice.data.sql.SqlQuery;
import com.nykredit.kundeservice.util.OperationAbortedException;

import absenceAnalysis.AbsenceAnalysisAgent;
import absenceAnalysis.AgentSicknessAbsenceResume;
import absenceAnalysis.ExemptionFromAnalysis;
import absenceAnalysis.SicknessNotification;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.jdesktop.swingx.JXDatePicker;
import org.joda.time.LocalDate;

import java.awt.Color;
import javax.swing.border.MatteBorder;
import java.awt.SystemColor;
import com.nykredit.kundeservice.swing.components.CalendarPanel;

import javax.swing.border.TitledBorder;

public class MainW extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	private final JButton btnGetData = new JButton("Hent workforce data");
	
	private ResumeListView lstSicknessResumes = new ResumeListView();
	private JButton btnCreateSicknessNotificationFromResume = new JButton("Opret samtale");
	
	private final JTabbedPane tabbedPaneSicknessResumeDetails = new JTabbedPane(JTabbedPane.TOP);
	
	private final JScrollPane sicknessResumesScrollPane = new JScrollPane();
	
	private final CalendarPanel sicknessCalendarPanel = new CalendarPanel(new LocalDate().minusMonths(12), new LocalDate());
	
	private final AgentListView lstAgent = new AgentListView();
	
	private final JLabel lblSelectedAgentInitials = new JLabel("-");
	private final JLabel lblSelectedAgentName = new JLabel("-");
	private final JLabel lblSelectedAgentTeam = new JLabel("-");
	private final JLabel lblSelectedAgentLeader = new JLabel("-");
	
	private final JList lstAgentNotifications = new JList();
	private final JComboBox cboNotificationResponsible = new JComboBox();
	private final JXDatePicker datePickerNotificationCreated = new JXDatePicker();
	private final JXDatePicker datePickerNotificationConfirmed = new JXDatePicker();
	private final JLabel txtSelectedNotificationLeader = new JLabel("-");
	private final JButton btnDeleteNotification = new JButton("Slet valgte");
	private final JButton btnNewNotification = new JButton("Opret ny");
	private final JButton btnSaveNotification = new JButton("Gem");
	
	private final JList lstAgentExemptions = new JList();
	private final JXDatePicker datePickerExemptionStart = new JXDatePicker();
	private final JXDatePicker datePickerExemptionEnd = new JXDatePicker();
	private final JTextArea txtExemptionNote = new JTextArea();
	private final JButton btnDeleteExemption = new JButton("Slet valgte");
	private final JButton btnNewExemption = new JButton("Opret ny");
	private final JButton btnSaveExemption = new JButton("Gem");
	
	private final JLabel lblExemptionSaved = new JLabel(new ImageIcon(MainW.class.getResource("/resources/Complete.png")), JLabel.CENTER);
	
	private ArrayList<AbsenceAnalysisAgent> agentData = new ArrayList<AbsenceAnalysisAgent>();
	private ArrayList<String> teamLeaderInitials = new ArrayList<String>();
			
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainW frame = new MainW();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void validate() {
		super.validate();
		
		this.tabbedPaneSicknessResumeDetails.setSize(this.getWidth() - 326, this.getHeight() - 125);

	}

	public MainW() {
		this.setTitle("Sygefrav\u00E6rssamtaler");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(200, 50, 972, 692);
		
		JTabbedPane tabbedPaneMain = new JTabbedPane(JTabbedPane.TOP);
		this.getContentPane().add(tabbedPaneMain, BorderLayout.CENTER);
		
		JDesktopPane panelSicknessResumes = new JDesktopPane();
		panelSicknessResumes.setOpaque(false);
		tabbedPaneMain.addTab("<html><body leftmargin=30 topmargin=3 marginwidth=25 marginheight=2>Check sygeperioder</body></html>", null, panelSicknessResumes, null);

		btnGetData.setBounds(10, 11, 148, 23);
		btnGetData.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.loadWorkForceData();
			}
		});
		panelSicknessResumes.add(btnGetData);
				
		this.lstSicknessResumes.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(e.getValueIsAdjusting() == false)
					MainW.this.currentResumeSelectionChanged(MainW.this.lstSicknessResumes.getSelectedResume());
			}
		});
		
		sicknessResumesScrollPane.setBounds(10, 45, 280, 505);
		sicknessResumesScrollPane.setViewportView(this.lstSicknessResumes);
		panelSicknessResumes.add(sicknessResumesScrollPane);
		
		JRadioButton rdbtnShowAllResumes = new JRadioButton("Vis alle");
		rdbtnShowAllResumes.setOpaque(false);
		rdbtnShowAllResumes.setBounds(10, 557, 57, 23);
		rdbtnShowAllResumes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.lstSicknessResumes.setFilter(ResumeFilter.SHOW_ALL);
			}
		});
		panelSicknessResumes.add(rdbtnShowAllResumes);
		
		JRadioButton rdbtnShowResumesDueForNotification = new JRadioButton("Vis overskredne");
		rdbtnShowResumesDueForNotification.setSelected(true);
		rdbtnShowResumesDueForNotification.setOpaque(false);
		rdbtnShowResumesDueForNotification.setBounds(105, 557, 109, 23);
		rdbtnShowResumesDueForNotification.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.lstSicknessResumes.setFilter(ResumeFilter.SHOW_PERIODLIMIT_EXCEEDED);
			}
		});
		panelSicknessResumes.add(rdbtnShowResumesDueForNotification);
		
		ButtonGroup resumeListFilter = new ButtonGroup();
		resumeListFilter.add(rdbtnShowAllResumes);
		resumeListFilter.add(rdbtnShowResumesDueForNotification);
		btnCreateSicknessNotificationFromResume.setEnabled(false);
		
		this.btnCreateSicknessNotificationFromResume.setBounds(10, 587, 100, 23);
		this.btnCreateSicknessNotificationFromResume.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.btnCreateSicknessNotificationFromResume_Clicked();
			}
		});
		panelSicknessResumes.add(this.btnCreateSicknessNotificationFromResume);
						
		JScrollPane scrollPaneSicknessCalendar = new JScrollPane(this.sicknessCalendarPanel);
		scrollPaneSicknessCalendar.getHorizontalScrollBar().setValue(scrollPaneSicknessCalendar.getHorizontalScrollBar().getMaximum());
		
		tabbedPaneSicknessResumeDetails.addTab("Kalender", null, scrollPaneSicknessCalendar, null);
		tabbedPaneSicknessResumeDetails.setBounds(300, 43, 646, 567);
		panelSicknessResumes.add(tabbedPaneSicknessResumeDetails);
		
		JScrollPane scrollPaneSicknessGraph = new JScrollPane();
		tabbedPaneSicknessResumeDetails.addTab("Graf", null, scrollPaneSicknessGraph, null);
		
		JDesktopPane desktopPaneHistory = new JDesktopPane();
		desktopPaneHistory.setOpaque(false);
				
		tabbedPaneMain.addTab("<html><body leftmargin=30 topmargin=3 marginwidth=25 marginheight=2>Historik</body></html>", null, desktopPaneHistory, null);
		
		lstAgent.setShowHorizontalLines(false);
		lstAgent.setFillsViewportHeight(true);
		lstAgent.setShowGrid(false);
		lstAgent.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting() == false)
					MainW.this.lstAgent_AgentSelected();
			}
		});
		
		JScrollPane agentListScrollPane = new JScrollPane(lstAgent);
		agentListScrollPane.setBounds(10, 45, 329, 421);
		desktopPaneHistory.add(agentListScrollPane);
		
		JRadioButton rdbtnShowAll = new JRadioButton("Vis alle");
		rdbtnShowAll.setSelected(true);
		rdbtnShowAll.setBounds(10, 473, 66, 23);
		rdbtnShowAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.lstAgent.setFilter(AgentFilter.SHOW_ALL);		
			}
		});
		rdbtnShowAll.setOpaque(false);
		desktopPaneHistory.add(rdbtnShowAll);
		
		JRadioButton rdbtnShowOnlyExemptions = new JRadioButton("Vis kun undtagelser");
		rdbtnShowOnlyExemptions.setBounds(91, 473, 119, 23);
		rdbtnShowOnlyExemptions.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.lstAgent.setFilter(AgentFilter.SHOW_ONLY_EXEMPTIONS);
			}
		});
		rdbtnShowOnlyExemptions.setOpaque(false);
		desktopPaneHistory.add(rdbtnShowOnlyExemptions);
		
		ButtonGroup agentListFilterOptions = new ButtonGroup();
		agentListFilterOptions.add(rdbtnShowAll);
		agentListFilterOptions.add(rdbtnShowOnlyExemptions);
		
		JDesktopPane desktopPaneAgentDetails = new JDesktopPane();
		desktopPaneAgentDetails.setBorder(new TitledBorder(null, "Agent - detaljer", TitledBorder.LEADING, TitledBorder.TOP, null, null));
				
		desktopPaneAgentDetails.setBounds(363, 45, 426, 412);
		desktopPaneAgentDetails.setOpaque(false);
		desktopPaneHistory.add(desktopPaneAgentDetails);
		
		JTabbedPane tabbedPaneAgentDetails = new JTabbedPane(JTabbedPane.TOP);
		tabbedPaneAgentDetails.setBounds(10, 104, 406, 302);
		desktopPaneAgentDetails.add(tabbedPaneAgentDetails);
		
		JDesktopPane desktopPaneAgentNotifications = new JDesktopPane();
		desktopPaneAgentNotifications.setOpaque(false);
		tabbedPaneAgentDetails.addTab("Notifikationer", null, desktopPaneAgentNotifications, null);
						
		JScrollPane scrollPaneAgentNotifications = new JScrollPane(this.lstAgentNotifications);
		scrollPaneAgentNotifications.setBounds(10, 11, 170, 218);
		desktopPaneAgentNotifications.add(scrollPaneAgentNotifications);
		
		this.btnNewNotification.setEnabled(false);
		this.btnNewNotification.setBounds(109, 244, 89, 23);
		this.btnNewNotification.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				MainW.this.createNotification();
			}
		});
		desktopPaneAgentNotifications.add(this.btnNewNotification);
		
		this.btnDeleteNotification.setEnabled(false);
		this.btnDeleteNotification.setBounds(10, 244, 89, 23);
		this.btnDeleteNotification.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.deleteCurrentNotification();
			}
		});
		desktopPaneAgentNotifications.add(this.btnDeleteNotification);
		
		this.btnSaveNotification.setEnabled(false);
		this.btnSaveNotification.setBounds(291, 244, 89, 23);
		this.btnSaveNotification.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.saveCurrentNotification();
			}
		});
		desktopPaneAgentNotifications.add(this.btnSaveNotification);
		
		JLabel lblNotificationCreated = new JLabel("Oprettet:");
		lblNotificationCreated.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNotificationCreated.setBounds(190, 26, 80, 14);
		desktopPaneAgentNotifications.add(lblNotificationCreated);
		
		this.datePickerNotificationCreated.setBorder(null);
		this.datePickerNotificationCreated.setEnabled(false);
		this.datePickerNotificationCreated.getEditor().setEditable(false);
		this.datePickerNotificationCreated.setBounds(280, 22, 100, 22);
		this.datePickerNotificationCreated.setFormats(new SimpleDateFormat("dd-MM-yyyy"));
		desktopPaneAgentNotifications.add(this.datePickerNotificationCreated);
		
		this.datePickerNotificationConfirmed.setBorder(null);
		this.datePickerNotificationConfirmed.setEnabled(false);
		this.datePickerNotificationConfirmed.getEditor().setEditable(false);
		this.datePickerNotificationConfirmed.setBounds(280, 55, 100, 22);
		this.datePickerNotificationConfirmed.setFormats(new SimpleDateFormat("dd-MM-yyyy"));
		desktopPaneAgentNotifications.add(this.datePickerNotificationConfirmed);
		
		JLabel lblNotificationConfirmed = new JLabel("M\u00F8de afholdt:");
		lblNotificationConfirmed.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNotificationConfirmed.setBounds(190, 59, 80, 14);
		desktopPaneAgentNotifications.add(lblNotificationConfirmed);
				
		JLabel lblNotificationLeader = new JLabel("Leder:");
		lblNotificationLeader.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNotificationLeader.setBounds(190, 92, 80, 14);
		desktopPaneAgentNotifications.add(lblNotificationLeader);
		
		txtSelectedNotificationLeader.setBounds(280, 92, 100, 14);
		desktopPaneAgentNotifications.add(txtSelectedNotificationLeader);
		
		JLabel lblNotificationResponsible = new JLabel("Ansvarlig");
		lblNotificationResponsible.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNotificationResponsible.setBounds(190, 125, 80, 14);
		desktopPaneAgentNotifications.add(lblNotificationResponsible);
		
		cboNotificationResponsible.setBounds(280, 121, 100, 22);
		desktopPaneAgentNotifications.add(this.cboNotificationResponsible);
		
		JLabel lblInitials = new JLabel("Initialer:");
		lblInitials.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblInitials.setBounds(28, 31, 60, 14);
		desktopPaneAgentDetails.add(lblInitials);
		
		this.lblSelectedAgentInitials.setBounds(98, 31, 54, 14);
		desktopPaneAgentDetails.add(this.lblSelectedAgentInitials);
		
		JLabel lblName = new JLabel("Navn:");
		lblName.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblName.setBounds(165, 31, 46, 14);
		desktopPaneAgentDetails.add(lblName);
		
		this.lblSelectedAgentName.setBounds(221, 31, 195, 14);
		desktopPaneAgentDetails.add(this.lblSelectedAgentName);
		
		JLabel lblTeam = new JLabel("Team:");
		lblTeam.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTeam.setBounds(28, 56, 46, 14);
		desktopPaneAgentDetails.add(lblTeam);
		
		this.lblSelectedAgentTeam.setBounds(98, 56, 46, 14);
		desktopPaneAgentDetails.add(this.lblSelectedAgentTeam);
		
		JLabel lblLeader = new JLabel("Leder:");
		lblLeader.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblLeader.setBounds(165, 56, 46, 14);
		desktopPaneAgentDetails.add(lblLeader);
		
		this.lblSelectedAgentLeader.setBounds(221, 56, 195, 14);
		desktopPaneAgentDetails.add(this.lblSelectedAgentLeader);
		
		JDesktopPane desktopPaneAgentExemptions = new JDesktopPane();
		
		desktopPaneAgentExemptions.setOpaque(false);
		tabbedPaneAgentDetails.addTab("Undtagelser", null, desktopPaneAgentExemptions, null);
		
		this.lstAgentExemptions.setBounds(10, 11, 170, 222);
		this.lstAgentExemptions.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(e.getValueIsAdjusting() == false)
					MainW.this.currentExemptionSelectionChanged();
			}
		});
		
		JScrollPane exemptionListScrollPane = new JScrollPane(this.lstAgentExemptions);
		exemptionListScrollPane.setBounds(10, 11, 170, 222);
		desktopPaneAgentExemptions.add(exemptionListScrollPane);
				
		JLabel lblExemptionStart = new JLabel("Start:");
		lblExemptionStart.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblExemptionStart.setBounds(190, 26, 46, 14);
		desktopPaneAgentExemptions.add(lblExemptionStart);
		
		JLabel lblExemptionEnd = new JLabel("Slut:");
		lblExemptionEnd.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblExemptionEnd.setBounds(190, 54, 38, 14);
		desktopPaneAgentExemptions.add(lblExemptionEnd);
		
		this.datePickerExemptionStart.getEditor().setEditable(false);
		this.datePickerExemptionStart.setBorder(null);
		this.datePickerExemptionStart.setEnabled(false);
		this.datePickerExemptionStart.setFormats(new SimpleDateFormat("dd-MM-yyyy"));
		this.datePickerExemptionStart.setBounds(280, 22, 100, 22);
		desktopPaneAgentExemptions.add(this.datePickerExemptionStart);
		
		this.datePickerExemptionEnd.getEditor().setEditable(false);
		this.datePickerExemptionEnd.setBorder(new EmptyBorder(1, 1, 1, 1));
		this.datePickerExemptionEnd.setEnabled(false);
		this.datePickerExemptionEnd.setFormats(new SimpleDateFormat("dd-MM-yyyy"));
		this.datePickerExemptionEnd.setBounds(280, 50, 100, 22);
		desktopPaneAgentExemptions.add(this.datePickerExemptionEnd);
		
		JLabel lblExemptionNote = new JLabel("Note:");
		lblExemptionNote.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblExemptionNote.setBounds(190, 82, 46, 14);
		desktopPaneAgentExemptions.add(lblExemptionNote);

		this.txtExemptionNote.setWrapStyleWord(true);
		this.txtExemptionNote.setLineWrap(true);
		this.txtExemptionNote.setBorder(new MatteBorder(1, 1, 1, 1, (Color) SystemColor.menu));
		this.txtExemptionNote.setColumns(10);
		this.txtExemptionNote.setEnabled(false);
		this.txtExemptionNote.setBounds(240, 79, 140, 154);
		this.txtExemptionNote.setFont(new Font("Verdana", Font.PLAIN, 10));
		
		JScrollPane scrollPaneExemptionNote = new JScrollPane(this.txtExemptionNote);
		scrollPaneExemptionNote.setBounds(240, 79, 140, 154);
				
		desktopPaneAgentExemptions.add(scrollPaneExemptionNote);
		
		this.btnDeleteExemption.setEnabled(false);
		this.btnDeleteExemption.setBounds(10, 244, 89, 23);
		this.btnDeleteExemption.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.deleteCurrentExemption();
			}
		});
		desktopPaneAgentExemptions.add(this.btnDeleteExemption);
		
		this.btnNewExemption.setEnabled(false);
		this.btnNewExemption.setBounds(109, 244, 89, 23);
		this.btnNewExemption.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.createExemption();
			}
		});
		desktopPaneAgentExemptions.add(this.btnNewExemption);
		
		this.btnSaveExemption.setEnabled(false);
		this.btnSaveExemption.setBounds(291, 244, 89, 23);
		this.btnSaveExemption.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainW.this.saveCurrentExemption();
			}
		});
		desktopPaneAgentExemptions.add(this.btnSaveExemption);
				
		this.lblExemptionSaved.setVisible(false);
		this.lblExemptionSaved.setBounds(265, 244, 23, 23);
		desktopPaneAgentExemptions.add(this.lblExemptionSaved);
		
		JDesktopPane desktopPaneReview = new JDesktopPane();
		desktopPaneReview.setOpaque(false);
		tabbedPaneMain.addTab("<html><body leftmargin=30 topmargin=3 marginwidth=25 marginheight=2>Opf\u00F8lgning</body></html>", null, desktopPaneReview, null);
		
		this.lstAgentNotifications.setBounds(10, 11, 170, 222);
		this.lstAgentNotifications.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting() == false)
					MainW.this.currentNotificationChanged();		
			}
		});
		
		try {
			this.retrieveLeaderInitials();
		} catch (DopeException e) {
			JOptionPane.showMessageDialog(this, "Kunne ikke indl�se teamchefer. Pr�v evt. at starte programmet igen. /n /n Detaljer: " + e.getDopeErrorMsg(), "Fejl.", JOptionPane.ERROR_MESSAGE);
		}
				
		try {
			KSDriftConnection conn = new KSDriftConnection();
			
			this.lstAgent.loadAgents(false, conn);
			this.sicknessCalendarPanel.LoadKundeserviceBusinessDates(conn);
			
			conn.closeConnection();
		} catch (OperationAbortedException e) {
			JOptionPane.showMessageDialog(this, "Kunne ikke indl�se agenter. Pr�v evt. at starte programmet igen. /n /n Detaljer: " + e.getDopeErrorMsg(), "Fejl.", JOptionPane.ERROR_MESSAGE);
		} catch (DopeDBException e) {
			JOptionPane.showMessageDialog(this, e.getDopeErrorMsg(), "Fejl", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void retrieveLeaderInitials() throws DopeDBException, DopeResultSetException{
		SqlQuery query = new SqlQuery(new KSDriftConnection(), "SELECT " + 
															       "* " +  
															   "FROM (" + 
															       "SELECT " +
															           "TEAM_LEDER " +
															       "FROM " +
															           "KS_DRIFT.AGENTER_TEAMS " +
															       "UNION SELECT " +
															           "TEAM_CHEFER " +
															       "FROM " + 
															           "KS_DRIFT.AGENTER_TEAMS) " +
															   "WHERE " +
															       "TEAM_LEDER is not null");
		
		query.execute();
		
		while(query.next()){
			this.teamLeaderInitials.add(query.getString("TEAM_LEDER"));
		}
	}
	private void loadWorkForceData(){
		this.agentData = new ArrayList<AbsenceAnalysisAgent>();
		
		try {
			this.agentData = this.lstAgent.loadAgents(true);
		} catch (OperationAbortedException ex) {
			JOptionPane.showMessageDialog(MainW.this, 
										  "Der skete en fejl ved indl�sning af workforcedata. /n /n Detaljer: " + ex.getDopeErrorMsg(), 
										  "Fejl", 
										  JOptionPane.ERROR_MESSAGE);
		}
				
		ArrayList<AgentSicknessAbsenceResume> resumes = new ArrayList<AgentSicknessAbsenceResume>();
		
		for(AbsenceAnalysisAgent a : MainW.this.agentData)
			resumes.add(a.getSicknessResume());
				
		this.lstSicknessResumes.setData(resumes);
	}
		
	private void btnCreateSicknessNotificationFromResume_Clicked(){
		if(this.lstSicknessResumes.getSelectedResume() != null){
			AgentSicknessAbsenceResume selectedResume = this.lstSicknessResumes.getSelectedResume();
			
			CreateNotification notificationDialog = new CreateNotification(selectedResume.getAgent(), this.teamLeaderInitials);
			notificationDialog.addActionListener(new createNotificationListener());			
			
			notificationDialog.setVisible(true);
		}
	}
	
	private void lstAgent_AgentSelected(){
		this.lblExemptionSaved.setVisible(false);
		
		AbsenceAnalysisAgent selectedAgent = this.lstAgent.getSelectedAgent();
		
		if (selectedAgent != null){
			this.lblSelectedAgentInitials.setText(selectedAgent.getInitials());
			this.lblSelectedAgentName.setText(selectedAgent.getFullName());
			this.lblSelectedAgentTeam.setText(selectedAgent.getTeam());
			this.lblSelectedAgentLeader.setText(selectedAgent.getLeaderInitials());

			this.btnNewNotification.setEnabled(true);
			this.lstAgentNotifications.setListData(selectedAgent.getNotifications().toArray());
			
			this.btnNewExemption.setEnabled(true);
			this.lstAgentExemptions.setListData(selectedAgent.getExemptions().toArray());
		} else {
			this.lblSelectedAgentInitials.setText("-");
			this.lblSelectedAgentName.setText("-");
			this.lblSelectedAgentTeam.setText("-");
			this.lblSelectedAgentLeader.setText("-");

			this.btnNewNotification.setEnabled(false);
			this.lstAgentNotifications.setListData(new SicknessNotification[0]);
			
			this.btnNewExemption.setEnabled(false);
			this.lstAgentExemptions.setListData(new ExemptionFromAnalysis[0]);
		}
		
		this.datePickerNotificationCreated.setEnabled(false);
		this.datePickerNotificationCreated.setDate(null);
		this.datePickerNotificationConfirmed.setEnabled(false);
		this.datePickerNotificationConfirmed.setDate(null);
		this.txtSelectedNotificationLeader.setText("-");
		this.cboNotificationResponsible.setEnabled(false);
		this.cboNotificationResponsible.setSelectedItem(-1);
		
		this.datePickerExemptionStart.setEnabled(false);
		this.datePickerExemptionStart.setDate(null);
		this.datePickerExemptionEnd.setEnabled(false);
		this.datePickerExemptionEnd.setDate(null);
		this.txtExemptionNote.setEnabled(false);
		this.txtExemptionNote.setText(null);
		this.btnDeleteExemption.setEnabled(false);
		this.btnNewExemption.setEnabled(true);
		this.btnSaveExemption.setEnabled(false);
	}
		
	private void validateNotificationDetails(){
		
	}
	private void deleteCurrentNotification(){
		
	}
	private void createNotification(){
		this.lstAgentNotifications.clearSelection();
		
		this.currentNotificationChanged();
	}
	private void saveCurrentNotification(){
		
	}
	
	private boolean validateExemptionDetails(){
		boolean validated = true;
		
		if(this.datePickerExemptionStart.getDate() == null){
			this.datePickerExemptionStart.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.RED));
			validated = false;
		}else{
			this.datePickerExemptionStart.setBorder(new EmptyBorder(0, 0, 0, 0));
			
			if(this.datePickerExemptionEnd.getDate() != null && 
			   this.datePickerExemptionEnd.getDate().before(this.datePickerExemptionStart.getDate())){
				this.datePickerExemptionEnd.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.RED));
				
				validated = false;
			}else{
				this.datePickerExemptionEnd.setBorder(new EmptyBorder(0, 0, 0, 0));
			}
			
		}
		
		if(this.txtExemptionNote.getText().length() > 1000){
			this.txtExemptionNote.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.RED));
			
			validated = false;
		} else
			this.txtExemptionNote.setBorder(new EmptyBorder(1, 1, 1, 1));	
		
		return validated;
	}
	private void deleteCurrentExemption(){
		if(this.lstAgentExemptions.getSelectedValue() instanceof ExemptionFromAnalysis){
			ExemptionFromAnalysis selectedExemption = (ExemptionFromAnalysis)this.lstAgentExemptions.getSelectedValue();
		
			try{
				selectedExemption.delete();				
			} catch(DopeDBException ex) {
				JOptionPane.showMessageDialog(MainW.this, 
	  					  					  "Der skete en fejl ved sletning af undtagelse. Det anbefales at du lukker programmet ned og starter forfra. /n /n Detaljer: " + ex.getDopeErrorMsg(), 
	  					  					  "Fejl", 
	  					  					  JOptionPane.ERROR_MESSAGE);
			}
			
			this.lstAgentExemptions.setListData(this.lstAgent.getSelectedAgent().getExemptions().toArray());
			this.lstAgentExemptions.validate();
			
			this.lstAgent.updateUI();
			this.lstSicknessResumes.updateList();
		}		
	}
	private void createExemption(){
		this.lstAgentExemptions.clearSelection();
		
		this.currentExemptionSelectionChanged();
	}
	private void saveCurrentExemption(){
		if(!this.validateExemptionDetails()){
			JOptionPane.showMessageDialog(MainW.this, 
					  					  "Se r�de markeringer.",
					  					  "Fejl i indtastning.", 
					  					  JOptionPane.INFORMATION_MESSAGE);
			
			return;
		}
		
		try {
			if (this.lstAgentExemptions.getSelectedValue() == null){
				ExemptionFromAnalysis newExemption = new ExemptionFromAnalysis(this.lstAgent.getSelectedAgent(), 
																			   LocalDate.fromDateFields(this.datePickerExemptionStart.getDate()), 
																			   (this.datePickerExemptionEnd.getDate() != null ? LocalDate.fromDateFields(this.datePickerExemptionEnd.getDate()) : null), 
																			   this.txtExemptionNote.getText());
				
				newExemption.save();
			} else if (this.lstAgentExemptions.getSelectedValue() instanceof ExemptionFromAnalysis) {
				ExemptionFromAnalysis selectedExemption = (ExemptionFromAnalysis)this.lstAgentExemptions.getSelectedValue();
				
				selectedExemption.setStartOfExemption(LocalDate.fromDateFields(this.datePickerExemptionStart.getDate()));
				selectedExemption.setEndOfExemption(LocalDate.fromDateFields(this.datePickerExemptionEnd.getDate()));
				selectedExemption.setNote(this.txtExemptionNote.getText());
				
				selectedExemption.save();
			}
			
			this.lblExemptionSaved.setVisible(true);
			
			this.lstAgentExemptions.setListData(this.lstAgent.getSelectedAgent().getExemptions().toArray());			
			this.lstAgentExemptions.validate();
			this.lstAgent.updateUI();
			this.lstSicknessResumes.updateList();
		} catch (DopeDBException e) {
			JOptionPane.showMessageDialog(MainW.this, 
					  					  "Der skete en fejl ved oprettelse af ny undtagelse. Det anbefales at du lukker programmet ned og starter forfra. \n \n Detaljer: " + e.getDopeErrorMsg(), 
					  					  "Fejl", 
					  					  JOptionPane.ERROR_MESSAGE);
		} catch (IllegalArgumentException e){
			JOptionPane.showMessageDialog(MainW.this, 
										  e.getMessage(),
										  "Fejl i indtastning.", 
										  JOptionPane.INFORMATION_MESSAGE);
		}
	}
		
	protected void currentResumeSelectionChanged(AgentSicknessAbsenceResume resume){
		this.sicknessCalendarPanel.clearCalendarMarkings();
		
		if(resume != null){
			this.sicknessCalendarPanel.addCalendarMarking("Sickness", this.sicknessCalendarPanel.new CalendarMarkings("Sygdom", resume.getSickDays(), Color.YELLOW));
			this.sicknessCalendarPanel.addCalendarMarking("DaysOff", this.sicknessCalendarPanel.new CalendarMarkings("Fri", resume.getDaysOff(), new Color(200, 200, 200)));
			
			this.btnCreateSicknessNotificationFromResume.setEnabled(true);
		}else
			this.btnCreateSicknessNotificationFromResume.setEnabled(false);
		
		this.sicknessCalendarPanel.updateMarkings();
	}

	protected void currentExemptionSelectionChanged(){
		ExemptionFromAnalysis currentExemption = (ExemptionFromAnalysis)MainW.this.lstAgentExemptions.getSelectedValue();
		
		this.datePickerExemptionStart.setEnabled(true);
		this.datePickerExemptionEnd.setEnabled(true);
		this.txtExemptionNote.setEnabled(true);
		
		if (currentExemption != null){
			this.datePickerExemptionStart.setDate(currentExemption.getStartOfExemption().toDate());
			this.datePickerExemptionEnd.setDate(currentExemption.getEndOfExemption() != null ? currentExemption.getEndOfExemption().toDate() : null);
			this.txtExemptionNote.setText(currentExemption.getNote());
			
			this.btnDeleteExemption.setEnabled(true);
			this.btnNewExemption.setEnabled(true);
		} else {
			this.datePickerExemptionStart.setDate(new Date());
			this.datePickerExemptionEnd.setDate(null);
			
			this.txtExemptionNote.setText(null);
			
			this.btnDeleteExemption.setEnabled(false);
			this.btnNewExemption.setEnabled(false);
		}
		
		this.btnSaveExemption.setEnabled(true);
	}
	protected void currentNotificationChanged(){
		SicknessNotification currentNotification = (SicknessNotification)MainW.this.lstAgentNotifications.getSelectedValue();
		
		this.datePickerNotificationCreated.setEnabled(true);
		this.datePickerNotificationConfirmed.setEnabled(true);
		this.cboNotificationResponsible.setEnabled(true);
		this.cboNotificationResponsible.setSelectedIndex(-1);
		//this.cboNotificationResponsible.
				
		if(currentNotification != null){
			this.datePickerNotificationCreated.setDate(currentNotification.getCreated().toDate());
			this.datePickerNotificationConfirmed.setDate(currentNotification.getConfirmed() != null ? currentNotification.getConfirmed().toDate() : null);
			this.txtSelectedNotificationLeader.setText(currentNotification.getLeaderInitials());
			
			if(!this.teamLeaderInitials.contains(currentNotification.getResponsibleForMeetingInitials()))
				this.cboNotificationResponsible.addItem(currentNotification.getResponsibleForMeetingInitials());
				
			this.cboNotificationResponsible.setSelectedItem(currentNotification.getResponsibleForMeetingInitials());
			
			this.btnDeleteNotification.setEnabled(true);
			this.btnNewNotification.setEnabled(true);
		} else {
			this.datePickerNotificationCreated.setDate(new Date());
			this.datePickerNotificationConfirmed.setDate(null);
			this.txtSelectedNotificationLeader.setText(this.lblSelectedAgentLeader.getText());
			this.cboNotificationResponsible.setSelectedItem(this.lblSelectedAgentLeader.getText());
			
			this.btnDeleteNotification.setEnabled(false);
			this.btnNewNotification.setEnabled(false);
		}
		
		this.btnSaveNotification.setEnabled(true);
	}
	
	private void saveNotificationFromDialog(SicknessNotification notification){
		try {
			notification.save();
			
			if (this.lstSicknessResumes.getFilter() == ResumeFilter.SHOW_PERIODLIMIT_EXCEEDED)
				this.lstSicknessResumes.updateList();			
		} catch (DopeException e) {
			JOptionPane.showMessageDialog(MainW.this, 
					  					  e.getMessage(),
					  					  "Fejl", 
					  					  JOptionPane.ERROR_MESSAGE);
		}
	}

	private class createNotificationListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			CreateNotification notificationDialog = (CreateNotification)e.getSource();
			
			if (e.getActionCommand().equalsIgnoreCase("Ok"))
				MainW.this.saveNotificationFromDialog(notificationDialog.getNotification());

			notificationDialog.setVisible(false);
			notificationDialog.dispose();
		}
	}
}